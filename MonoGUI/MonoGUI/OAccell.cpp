////////////////////////////////////////////////////////////////////////////////
// @file OAccell.cpp
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#include "MonoGUI.h"

OAccell::OAccell()
{
	m_nCount = 0;
	m_pAccell = NULL;
}

OAccell::~OAccell()
{
	// 删除快捷键列表
	RemoveAll ();
}

// 初始化快捷键列表
BOOL OAccell::Create (char* sAccellTable, KeyMap* pKeyMap)
{
	RemoveAll ();
	char sKeyName[256];
	char sTemp[256];

	ACCELL* theNext = m_pAccell;

	int k = 0;
	BOOL bFirst = TRUE;

	while (1)
	{
		memset (sKeyName, 0x0, sizeof(sKeyName));
		if (! GetSlice (sAccellTable, sKeyName, sizeof(sKeyName), k))
		{
			return TRUE;
		}

		int nKeyValue = pKeyMap->Find (sKeyName);
		if (this->Find (nKeyValue) != -1) {
			return FALSE;	// 该KeyValue已经存在，不能再添加了
		}
	    
		k ++;

		memset (sTemp, 0x0, sizeof(sTemp));
		if (! GetSlice (sAccellTable, sTemp, sizeof(sTemp), k))
		{
			return FALSE;	// 不成组
		}

		k ++;

		int nButtonID = atoi(sTemp);

		m_nCount ++;
		ACCELL* theNewOne = new ACCELL;
		theNewOne->nKeyValue = nKeyValue;
		theNewOne->id = nButtonID;

		if (bFirst)	// 如果是第一次，新建表项的指针赋给m_pAccell
		{
			m_pAccell = theNewOne;
			theNext = m_pAccell;
			bFirst = FALSE;
		}
		else
		{
			theNext->next = theNewOne;
			theNext = theNewOne;
		}
	}
}

// 搜索快捷键列表
int OAccell::Find (int nKeyValue)
{
	ACCELL* theCurrent = m_pAccell;
	
	int i;
	for (i = 0; i < m_nCount; i++)
	{
		if (theCurrent->nKeyValue == nKeyValue)
		{
		    return theCurrent->id;
		}
		theCurrent = theCurrent->next;
	}
	return -1;
}

BOOL OAccell::RemoveAll ()
{
	ACCELL* theNext = NULL;

	int i;
	for (i = 0; i < m_nCount; i++)
	{
		if (m_pAccell == NULL)
		{
			m_nCount = 0;
			return FALSE;
		}

		theNext = m_pAccell->next;
		if (m_pAccell != NULL)
		{
			delete m_pAccell;
		}
		m_pAccell = theNext;
	}
	m_nCount = 0;
	return TRUE;
}

/* END */
