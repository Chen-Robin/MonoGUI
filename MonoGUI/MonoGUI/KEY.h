////////////////////////////////////////////////////////////////////////////////
// @file KEY.h: define all KEY_ key value;
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

// 本文件定义了全部键值
#if !defined(__KEY_H__)
#define __KEY_H__

#if defined (RUN_ENVIRONMENT_WIN32)
// 定义Win32环境下的键值

// 数字键
#define KEY_0				0X30 
#define KEY_1				0X31
#define KEY_2				0X32
#define KEY_3				0X33
#define KEY_4				0X34
#define KEY_5				0X35
#define KEY_6				0X36
#define KEY_7				0X37
#define KEY_8				0X38
#define KEY_9				0X39
#define KEY_00				0XA1 // VK_RSHIFT

// 字母键
#define KEY_A               0X41
#define KEY_B               0X42
#define KEY_C               0X43
#define KEY_D               0X44
#define KEY_E               0X45
#define KEY_F               0X46
#define KEY_G               0X47
#define KEY_H               0X48
#define KEY_I               0X49
#define KEY_J               0X4A
#define KEY_K               0X4B
#define KEY_L               0X4C
#define KEY_M               0X4D
#define KEY_N               0X4E
#define KEY_O               0X4F
#define KEY_P               0X50
#define KEY_Q               0X51
#define KEY_R               0X52
#define KEY_S               0X53
#define KEY_T               0X54
#define KEY_U               0X55
#define KEY_V               0X56
#define KEY_W               0X57
#define KEY_X               0X58
#define KEY_Y               0X59
#define KEY_Z               0X5A

// 功能键
#define KEY_F1				0X70
#define KEY_F2				0X71
#define KEY_F3              0X72
#define KEY_F4              0X73
#define KEY_F5              0X74
#define KEY_F6              0X75
#define KEY_F7              0X76
#define KEY_F8              0X77
#define KEY_F9              0X78
#define KEY_F10             0X79
#define KEY_F11             0X7A
#define KEY_F12             0X7B

// 方向键
#define KEY_LEFT            0X25
#define KEY_UP              0x26
#define KEY_RIGHT           0X27
#define KEY_DOWN            0X28

// 符号键
#define KEY_SUB             0XBD	// (-_)
#define KEY_EQUALS          0XBB	// (=+)
#define KEY_SEMICOLON       0XBA	// (;:)
#define KEY_QUOTATION       0XDE    // ('")
#define KEY_BACK_QUOTE      0XC0	// (`~)
#define KEY_BACK_SLASH      0XDC	// (\|)
#define KEY_DIVIDE          0XBF	// (/?)
#define KEY_COMMA           0XBC	// (,<)
#define KEY_PERIOD          0XBE	// (.>)
#define KEY_OPEN_BRACKET    0XDB	// ([{)
#define KEY_CLOSE_BRACKET   0XDD	// (]})

// 上档键转换后的字符
// (Windows下不会产生这些按键，给他们一些不会重复的值即可)
#define KEY_SUB_S           0XF1	// (_)
#define KEY_EQUALS_S        0XF2	// (+)
#define KEY_SEMICOLON_S     0XF3	// (:)
#define KEY_QUOTATION_S     0XF4	// (")
#define KEY_BACK_QUOTE_S    0XF5	// (~)
#define KEY_BACK_SLASH_S    0XF6	// (|)
#define KEY_DIVIDE_S        0XF7	// (?)
#define KEY_COMMA_S         0XF8	// (<)
#define KEY_PERIOD_S        0XF9	// (>)
#define KEY_OPEN_BRACKET_S  0XFA	// ({)
#define KEY_CLOSE_BRACKET_S 0XFB	// (})

// (Windows下不会产生这些按键，给他们一些不会重复的值即可)
#define KEY_0_S				0XE0	// ')'
#define KEY_1_S				0XE1	// (!)
#define KEY_2_S				0XE2	// (@)
#define KEY_3_S				0XE3	// (#)
#define KEY_4_S				0XE4	// ($)
#define KEY_5_S				0XE5	// (%)
#define KEY_6_S				0XE6	// (^)
#define KEY_7_S				0XE7	// (&)
#define KEY_8_S				0XE8	// (*)
#define KEY_9_S				0XE9	// '('

// 编辑控制键
#define KEY_SPACE           0X20
#define KEY_BACK_SPACE      0X08
#define KEY_TAB             0x09
#define KEY_INSERT          0X2D
#define KEY_DELETE          0X2E
#define KEY_HOME            0X24
#define KEY_END             0X23
#define KEY_PAGE_UP         0X21
#define KEY_PAGE_DOWN       0X22

// 其他按键
#define KEY_ENTER           0X0D
#define KEY_ESCAPE          0X1B
#define KEY_CONTROL         0X11
#define KEY_SHIFT           0X10
#define KEY_PAUSE           0X13	// (BREAK)
#define KEY_CAPS_LOCK       0X14
#define KEY_NUM_LOCK        0X90

#endif	// Win32环境下的键值定义结束

#if defined(RUN_ENVIRONMENT_LINUX)
// 定义linux环境下的键值（请根据具体的嵌入式系统环境进行修改）
//#include <X11/keysymdef.h>

// 数字键
#define KEY_0				0X0030 
#define KEY_1				0X0031
#define KEY_2				0X0032
#define KEY_3				0X0033
#define KEY_4				0X0034
#define KEY_5				0X0035
#define KEY_6				0X0036
#define KEY_7				0X0037
#define KEY_8				0X0038
#define KEY_9				0X0039
#define KEY_00				0XFFE4  // right control

// 字母键
#define KEY_A               0X0041
#define KEY_B               0X0042
#define KEY_C               0X0043
#define KEY_D               0X0044
#define KEY_E               0X0045
#define KEY_F               0X0046
#define KEY_G               0X0047
#define KEY_H               0X0048
#define KEY_I               0X0049
#define KEY_J               0X004A
#define KEY_K               0X004B
#define KEY_L               0X004C
#define KEY_M               0X004D
#define KEY_N               0X004E
#define KEY_O               0X004F
#define KEY_P               0X0050
#define KEY_Q               0X0051
#define KEY_R               0X0052
#define KEY_S               0X0053
#define KEY_T               0X0054
#define KEY_U               0X0055
#define KEY_V               0X0056
#define KEY_W               0X0057
#define KEY_X               0X0058
#define KEY_Y               0X0059
#define KEY_Z               0X005A

// 功能键
#define KEY_F1				0XFFBE
#define KEY_F2				0XFFBF
#define KEY_F3              0XFFC0
#define KEY_F4              0XFFC1
#define KEY_F5              0XFFC2
#define KEY_F6              0XFFC3
#define KEY_F7              0XFFC4
#define KEY_F8              0XFFC5
#define KEY_F9              0XFFC6
#define KEY_F10             0XFFC7
#define KEY_F11             0XFFC8
#define KEY_F12             0XFFC9

// 方向键
#define KEY_LEFT            0XFF51
#define KEY_UP              0XFF52
#define KEY_RIGHT           0XFF53
#define KEY_DOWN            0XFF54

// 符号键
#define KEY_SUB             0X002D	// (-)
#define KEY_EQUALS          0X003D	// (=)
#define KEY_SEMICOLON       0X003B	// (;)
#define KEY_QUOTATION       0X0027  // (')
#define KEY_BACK_QUOTE      0X0060	// (`)
#define KEY_BACK_SLASH      0X005C	// (\)
#define KEY_DIVIDE          0X002F	// (/)
#define KEY_COMMA           0X002C	// (,)
#define KEY_PERIOD          0X002E	// (.)
#define KEY_OPEN_BRACKET    0X005B	// ([)
#define KEY_CLOSE_BRACKET   0X005D	// (])

#define KEY_SUB_S           0X005F	// (_)
#define KEY_EQUALS_S        0X002B	// (+)
#define KEY_SEMICOLON_S     0X003A	// (:)
#define KEY_QUOTATION_S     0X0022  // (")
#define KEY_BACK_QUOTE_S    0X007E	// (~)
#define KEY_BACK_SLASH_S    0X007C	// (|)
#define KEY_DIVIDE_S        0X003F	// (?)
#define KEY_COMMA_S         0X003C	// (<)
#define KEY_PERIOD_S        0X003E	// (>)
#define KEY_OPEN_BRACKET_S  0X007B	// ({)
#define KEY_CLOSE_BRACKET_S 0X007D	// (})

#define KEY_0_S				0X0029  // ')'
#define KEY_1_S				0X0011  // (!)
#define KEY_2_S				0X0040  // (@)
#define KEY_3_S				0X0023  // (#)
#define KEY_4_S				0X0024  // ($)
#define KEY_5_S				0X0025  // (%)
#define KEY_6_S				0X005E  // (^)
#define KEY_7_S				0X0026  // (&)
#define KEY_8_S				0X002A  // (*)
#define KEY_9_S				0X0028  // '('

// 编辑控制键
#define KEY_SPACE           0X0020
#define KEY_BACK_SPACE      0XFF08
#define KEY_TAB             0xFF09
#define KEY_INSERT          0XFF9E
#define KEY_DELETE          0XFF9F
#define KEY_HOME            0XFF50
#define KEY_END             0XFF57
#define KEY_PAGE_UP         0XFF55
#define KEY_PAGE_DOWN       0XFF56

// 其他按键
#define KEY_ENTER           0XFF0D
#define KEY_ESCAPE          0XFF1B
#define KEY_CONTROL         0XFFE3  // left control
#define KEY_CONTROL_R       0XFFE4  // right control
#define KEY_SHIFT           0XFFE1  // left shift
#define KEY_SHIFT_R         0XFFE2  // right shift
#define KEY_PAUSE           0XFF13	// (BREAK)
#define KEY_CAPS_LOCK       0XFFE5
#define KEY_NUM_LOCK        0XFF7F

#endif	// linux环境下的键值定义结束

// 特殊按键掩码
#define SHIFT_MASK          0x01
#define CAPSLOCK_MASK       0x10

#endif // !defined(__KEY_H__)

