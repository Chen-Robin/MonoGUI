////////////////////////////////////////////////////////////////////////////////
// @file OCombo.cpp
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#include "MonoGUI.h"

OCombo::OCombo()
	: OWindow(self_type)
{
	m_pEdit = new OEdit();
	m_pList = new OList();
}

OCombo::~OCombo()
{
	// 由于Edit和List都是自己的子窗口，OWindow将按照默认的规则进行销毁
	// 如果Edit和List没有经过Create挂载，则需要删除
	if (m_pEdit->m_pParent == NULL)
		delete m_pEdit;
	if (m_pList->m_pParent == NULL)
		delete m_pList;
}

// 创建组合框
BOOL OCombo::Create (OWindow* pParent,WORD wStyle,WORD wStatus,
					 int x,int y,int w,int h,int ID)
{
	m_pEdit->m_wStyle = WND_STYLE_SOLID;
	m_pEdit->m_wStatus = WND_STATUS_NORMAL;
	m_pEdit->m_x = x;
	m_pEdit->m_y = y;
	m_pEdit->m_w = w - COMBO_PUBHDOWN_BUTTON_WIDTH - 1;
	m_pEdit->m_h = h;
	m_pEdit->m_ID = ID;
	m_pEdit->m_pApp = pParent->m_pApp;

	// List的位置要重新计算一下，看看向下弹还是向上弹
	int theY = y + h - 1;
	if (theY + COMBO_DEFAULT_LIST_HEIGHT > SCREEN_H) {
		theY = y - COMBO_DEFAULT_LIST_HEIGHT + 1;
	}
	m_pList->m_wStyle = WND_STYLE_SOLID;
	m_pList->m_wStatus = WND_STATUS_NORMAL;
	m_pList->m_x = x;
	m_pList->m_y = theY;
	m_pList->m_w = w;
	m_pList->m_h = COMBO_DEFAULT_LIST_HEIGHT;
	m_pList->m_ID = COMBO_ID_LIST;
	m_pList->m_pApp = pParent->m_pApp;

	if (!OWindow::Create(pParent, wStyle, wStatus, x, y, w, h, ID)) {
		DebugPrintf("ERROR: OCombo::Create OWindow::Create fail!\n");
		return FALSE;
	}

	return TRUE;
}

// 绘制组合框
void OCombo::Paint (LCD* pLCD)
{
	CHECK_TYPE;

	// 如果不可见，则什么也不绘制
	if (! IsWindowVisible()) {
		return;
	}

	m_pEdit->Paint (pLCD);

	// 画出右侧的向下箭头及方框
	int crBk = 1;
	int crFr = 0;
	pLCD->FillRect(m_x+m_w-COMBO_PUBHDOWN_BUTTON_WIDTH-1, m_y, COMBO_PUBHDOWN_BUTTON_WIDTH, m_h, crBk);
	pLCD->HLine   (m_x+m_w-COMBO_PUBHDOWN_BUTTON_WIDTH-1, m_y, COMBO_PUBHDOWN_BUTTON_WIDTH, crFr);
	pLCD->HLine   (m_x+m_w-COMBO_PUBHDOWN_BUTTON_WIDTH-1, m_y+m_h-1, COMBO_PUBHDOWN_BUTTON_WIDTH, crFr);
	pLCD->VLine   (m_x+m_w-1, m_y, m_h, crFr);

	// 如果下拉列表没有打开，则绘制实心的箭头，否则绘制空心的箭头
	if( m_dwAddData2 == COMBO_UN_DROPPED ) {
		pLCD->DrawImage(m_x+m_w-10, m_y+m_h-9, 7, 7, g_4color_Arror_Down, 0, 0, LCD_MODE_NORMAL);
	}
	else {
		pLCD->DrawImage(m_x+m_w-10, m_y+m_h-9, 7, 7, g_4color_Hollow_Arror_Down, 0, 0, LCD_MODE_NORMAL);
	}
}

// 组合框消息处理
int OCombo::Proc (OWindow* pWnd, ULONGLONG nMsg, ULONGLONG wParam, ULONGLONG lParam)
{
	CHECK_TYPE_RETURN;

	if (! IsWindowEnabled()) {
		return 0;
	}

	int nReturn = 0;

	if (nMsg == OM_KEYDOWN)
	{
		switch (wParam)
		{
		case KEY_ESCAPE:
			{
				// 如果List处于打开状态，则关闭List，并修改iReturn为1
				if (m_dwAddData2 == COMBO_DROPPED)
				{
					ShowDropDown (FALSE);
					nReturn = 1;
				}
				// 如果List未打开，则交给Edit去处理(输入法需要响应该消息)
				else
				{
					if (CanEdit())
					{
						m_pEdit->Proc (pWnd, nMsg, wParam, lParam);

						if (m_pEdit->IsIMEOpen()) {
					        nReturn = 1;
						}
					}
				}
			}
			break;

		case KEY_UP:
			{
				// 如果List处于打开状态，则将此消息交给List处理
				if (m_dwAddData2 == COMBO_DROPPED)
				{
					nReturn = m_pList->Proc (pWnd, nMsg, wParam, lParam);
				}
				// 如果List未打开，则交给Edit去处理(输入法需要响应该消息)
				else
				{
					if (CanEdit())
					{
						nReturn = m_pEdit->Proc (pWnd, nMsg, wParam, lParam);
					}
					// 如果Edit未处理，则根据List更新Edit
					if (nReturn == 0)
					{
						nReturn = m_pList->Proc (pWnd, nMsg, wParam, lParam);
						SyncString();
					}
				}
			}
			break;

		case KEY_DOWN:
			{
				// 如果List处于打开状态，则将此消息交给List处理
				if (m_dwAddData2 == COMBO_DROPPED)
				{
					nReturn = m_pList->Proc (pWnd, nMsg, wParam, lParam);
				}
				// 如果List未打开，则交给Edit去处理(输入法需要响应该消息)
				else
				{
					if (CanEdit())
					{
						nReturn = m_pEdit->Proc (pWnd, nMsg, wParam, lParam);
					}
					// 如果Edit未处理，则根据List更新Edit
					if (nReturn == 0)
					{
						nReturn = m_pList->Proc (pWnd, nMsg, wParam, lParam);
						SyncString();
					}
				}
			}
			break;

		case KEY_LEFT:
			{
				// 如果List处于关闭状态，则将此消息交给Edit处理
				if (m_dwAddData2 == COMBO_UN_DROPPED)
				{
					if (CanEdit())
					{
						nReturn = m_pEdit->Proc (pWnd, nMsg, wParam, lParam);
					}
				}
			}
			break;

		case KEY_RIGHT:
			{
				// 如果List处于关闭状态，则将此消息交给Edit处理
				if (m_dwAddData2 == COMBO_UN_DROPPED)
				{
					if (CanEdit())
					{
						nReturn = m_pEdit->Proc (pWnd, nMsg, wParam, lParam);
					}
				}
			}
			break;

		case KEY_HOME:
			{
				// 如果List处于关闭状态，则将此消息交给Edit处理
				if (m_dwAddData2 == COMBO_UN_DROPPED)
				{
					if (CanEdit())
					{
						m_pEdit->Proc (pWnd, nMsg, wParam, lParam);
					}
				}
				nReturn = 1;
			}
			break;

		case KEY_END:
			{
				// 如果List处于关闭状态，则将此消息交给Edit处理
				if (m_dwAddData2 == COMBO_UN_DROPPED)
				{
					if (CanEdit())
					{
						m_pEdit->Proc (pWnd, nMsg, wParam, lParam);
					}
				}
				nReturn = 1;
			}
			break;

		case KEY_BACK_SPACE:	// 删除当前插入位置前面的字符
			{
				// 如果List处于关闭状态，则将此消息交给Edit处理
				if (m_dwAddData2 == COMBO_UN_DROPPED)
				{
					if (CanEdit())
					{
						m_pEdit->Proc (pWnd, nMsg, wParam, lParam);
					}
				}
				nReturn = 1;
			}
			break;

		case KEY_DELETE:		// 删除当前插入位置后面的字符
			{
				// 如果List处于关闭状态，则将此消息交给Edit处理
				if (m_dwAddData2 == COMBO_UN_DROPPED)
				{
					if (CanEdit())
					{
						m_pEdit->Proc (pWnd, nMsg, wParam, lParam);
					}
				}
				nReturn = 1;
			}
			break;

		case KEY_IME_ONOFF:		// 打开关闭输入法
			{
				// 如果List处于关闭状态，则将此消息交给Edit处理
				if (m_dwAddData2 == COMBO_UN_DROPPED)
				{
					if (CanEdit())
					{
						m_pEdit->Proc (pWnd, nMsg, wParam, lParam);
					}
				}
				nReturn = 1;
			}
			break;

		case KEY_IME_NEXT:
		case KEY_IME_PREV:		// 切换输入法
			{
				// 如果List处于关闭状态，则将此消息交给Edit处理
				if (m_dwAddData2 == COMBO_UN_DROPPED)
				{
					if (CanEdit())
					{
						m_pEdit->Proc (pWnd, nMsg, wParam, lParam);
					}
				}
				nReturn = 1;
			}
			break;

		case KEY_ENTER:			// 传送给输入法的确认键
			{
				// 如果List处于关闭状态，则将此消息交给Edit处理
				if (m_dwAddData2 == COMBO_UN_DROPPED)
				{
					if (CanEdit())
					{
						nReturn = m_pEdit->Proc (pWnd, nMsg, wParam, lParam);
					}

					if (nReturn == 0)
					{
						// 如果Edit没有处理，则打开列表框
						if (ShowDropDown(TRUE)) {
							nReturn = 1;
						}
					}
				}
				else
				{
					// 根据列表框的内容更新编辑框，并关闭列表框
					SyncString();
					ShowDropDown (FALSE);
					nReturn = 1;
				}
			}
			break;
		case KEY_0:				// 数字键和小数点
		case KEY_1:
		case KEY_2:
		case KEY_3:
		case KEY_4:
		case KEY_5:
		case KEY_6:
		case KEY_7:
		case KEY_8:
		case KEY_9:
		case KEY_PERIOD:
		case KEY_SPACE:
			{
				// 如果List处于关闭状态，则将此消息交给Edit处理
				if (m_dwAddData2 == COMBO_UN_DROPPED)
				{
					if (CanEdit())
					{
						m_pEdit->Proc (pWnd, nMsg, wParam, lParam);
					}
				}
				nReturn = 1;
			}
			break;

		default:
			{

#if defined (CHINESE_SUPPORT)
				// 如果List处于关闭状态，则将此消息交给Edit处理
				if (m_dwAddData2 == COMBO_UN_DROPPED)
				{
					if (CanEdit())
					{
						m_pEdit->Proc (pWnd, nMsg, wParam, lParam);
					}
				}
				nReturn = 1;
#endif // defined(CHINESE_SUPPORT)

			}
		}
	}
	else if (nMsg == OM_SETFOCUS)
	{
		// 得到焦点，首先判断是否应自动弹出下拉列表
		if (pWnd == this)
		{
			if ((m_wStyle & WND_STYLE_AUTO_DROPDOWN) > 0)
			{
				// 弹出下拉菜单
				ShowDropDown(TRUE);
			}
			else
			{
				EditSetFocus();
			}
		}
	}
	else if (nMsg == OM_KILLFOCUS)
	{
		// 失去焦点，关闭List
		if (pWnd == this) {
			ShowDropDown(FALSE);
		}

		// 修改Edit和List的status，失去焦点
		EditKillFocus();
		ListKillFocus();
	}

	UpdateView (this);
	return nReturn;
}

#if defined (MOUSE_SUPPORT)
// 坐标设备消息处理
int OCombo::PtProc (OWindow* pWnd, ULONGLONG nMsg, ULONGLONG wParam, ULONGLONG lParam)
{
	CHECK_TYPE_RETURN;

	if (! IsWindowEnabled()) {
		return 0;
	}

	int nReturn = OWindow::PtProc (pWnd, nMsg, wParam, lParam);

	if (nMsg == OM_LBUTTONDOWN)
	{
		int x = wParam;
		int y = lParam;
		if (PtInWindow(wParam, lParam))
		{
			if (m_pEdit->PtInWindow(x, y))
			{
				// 编辑框处理
				nReturn = m_pEdit->PtProc (pWnd, nMsg, wParam, lParam);
				if (m_dwAddData2 == COMBO_DROPPED) {
					ShowDropDown(FALSE);
				}
			}
			else
			{
				// 弹出\关闭下拉列表框
				if (m_dwAddData2 == COMBO_UN_DROPPED)
					ShowDropDown(TRUE);
				else
					ShowDropDown(FALSE);
			}

			nReturn = 1;
		}
		else if (m_dwAddData2 == COMBO_DROPPED)
		{
			// 下拉列表框处理
			nReturn = m_pList->PtProc (pWnd, nMsg, wParam, lParam);
			if (nReturn == 1)
			{
				if (m_pList->PtInItems(x, y) != -1)
				{
					// 鼠标选中列表区关闭下拉列表
					SyncString();
					ShowDropDown(FALSE);
					nReturn = 1;
				}
			}
			else {
				ShowDropDown(FALSE);
			}
		}
	}
	else if ((nMsg == OM_LBUTTONUP) ||
			 (nMsg == OM_MOUSEMOVE))
	{
		nReturn = m_pEdit->PtProc (pWnd, nMsg, wParam, lParam);
		if (nReturn != 1) {
			nReturn = m_pList->PtProc (pWnd, nMsg, wParam, lParam);
		}
	}

	return nReturn;
}
#endif // defined(MOUSE_SUPPORT)

// 显示或者隐藏下拉列表框
BOOL OCombo::ShowDropDown (BOOL bShow)
{
	CHECK_TYPE_RETURN;

	// 如果当前设置禁止弹出下拉列表框，则返回FALSE
	if (! CanDropDown())
	{
		if (m_dwAddData2 == COMBO_DROPPED)
		{
			// 隐藏下拉列表
			m_pList->m_wStatus |= WND_STATUS_INVISIBLE;
			m_pApp->SetTopMost(NULL);
			m_dwAddData2 = COMBO_UN_DROPPED;
		}
		return FALSE;
	}
	else
	{
		if (bShow)
		{
			// 显示下拉列表
			m_pList->m_wStatus &= ~WND_STATUS_INVISIBLE;
			DebugPrintf("OCombo::ShowDropDown: pWindow->type is %s\n",
				GetWindowTypeName(m_pList->GetWndType()));
			m_pApp->SetTopMost(m_pList);
			m_dwAddData2 = COMBO_DROPPED;
			// 将下拉菜单设置为焦点
			ListSetFocus();
		}
		else
		{
			// 隐藏下拉列表
			m_pList->m_wStatus |= WND_STATUS_INVISIBLE;
			m_pApp->SetTopMost(NULL);
			m_dwAddData2 = COMBO_UN_DROPPED;
			// 使下拉菜单失去焦点
			ListKillFocus();
		}
	}

	return TRUE;
}

// 得到下拉列表框的显示状态
BOOL OCombo::GetDroppedState()
{
	CHECK_TYPE_RETURN;

	if (m_dwAddData2 == COMBO_DROPPED) {
		return TRUE;
	}
	else {
		return FALSE;
	}
}

// 设置下拉列表框的高度(以行数计)
BOOL OCombo::SetDroppedLinage (int nLinage)
{
	CHECK_TYPE_RETURN;

	if (nLinage < 1) {
		return FALSE;
	}

	// 设置下拉列表框可以显示出来的行数
	m_pList->m_dwAddData4 = nLinage;

	// 设置下拉列表框的高度
	int theH = (HZK_H + 1) * nLinage + 3;

	// List的位置要重新计算一下，看看向下弹还是向上弹
	int theY = m_y + m_h - 1;
	if (theY + theH > SCREEN_H) {
		theY = m_y - theH + 1;
	}

	// 更新List的窗口大小
	m_pList->SetPosition (m_pList->m_x, theY, m_pList->m_w, theH);

	// 更新List的滚动条
	m_pList->RenewScroll();

	return TRUE;
}

// 允许或者禁止对编辑框输入文字
BOOL OCombo::EnableEdit (BOOL bEnable)
{
	CHECK_TYPE_RETURN;

	if (bEnable) {
		m_dwAddData1 &= ~COMBO_DISABLE_EDIT;
	}
	else {
		m_dwAddData1 |= COMBO_DISABLE_EDIT;
	}

	return TRUE;
}

// 允许或者禁止下拉菜单弹出
BOOL OCombo::EnableDropDown (BOOL bEnable)
{
	CHECK_TYPE_RETURN;

	if (bEnable) {
		m_dwAddData1 &= ~COMBO_DISABLE_DROPDOWN;
	}
	else {
		m_dwAddData1 |= COMBO_DISABLE_DROPDOWN;
	}

	return TRUE;
}

// 以下函数用于操作编辑框
// 限制文字的长度
int OCombo::LimitText (int nLength)
{
	CHECK_TYPE_RETURN;

	return m_pEdit->LimitText (nLength);
}

// 清空编辑框
BOOL OCombo::Clean()
{
	CHECK_TYPE_RETURN;

	return m_pEdit->Clean();
}

// 取得编辑框的文字
BOOL OCombo::GetText (char* pText)
{
	CHECK_TYPE_RETURN;

	return m_pEdit->GetText (pText);
}

// 设置编辑框的文字
BOOL OCombo::SetText (char* pText, int nLength)
{
	CHECK_TYPE_RETURN;

	return m_pEdit->SetText (pText, nLength);
}

// 取得编辑框文字的长度
int OCombo::GetTextLength()
{
	CHECK_TYPE_RETURN;

	return m_pEdit->GetTextLength();
}

// 以下函数用于操作下拉列表框
// 得到列表的条目数
int OCombo::GetCount()
{
	CHECK_TYPE_RETURN;

	return m_pList->GetCount();
}

// 得到列表框当前选中项目的Index，如果没有选中的则返回-1
int OCombo::GetCurSel()
{
	CHECK_TYPE_RETURN;

	return m_pList->GetCurSel();
}

// 设置列表框当前的选中项目
int OCombo::SetCurSel (int nIndex)
{
	CHECK_TYPE_RETURN;

	return m_pList->SetCurSel (nIndex);
}

// 获得某一列表项的内容
BOOL OCombo::GetString (int nIndex, char* pText)
{
	CHECK_TYPE_RETURN;

	return m_pList->GetString (nIndex, pText);
}

// 设置某一列表项的内容
BOOL OCombo::SetString (int nIndex, char* pText)
{
	CHECK_TYPE_RETURN;

	return m_pList->SetString (nIndex, pText);
}

// 获得某一列表项内容的长度
int OCombo::GetStringLength (int nIndex)
{
	CHECK_TYPE_RETURN;

	return m_pList->GetStringLength (nIndex);
}

// 向列表框中添加一个串(加在末尾)
BOOL OCombo::AddString (char* pText)
{
	CHECK_TYPE_RETURN;

	return m_pList->AddString (pText);
}

// 删除列表框的一个列表项
BOOL OCombo::DeleteString (int nIndex)
{
	CHECK_TYPE_RETURN;

	return m_pList->DeleteString (nIndex);
}

// 在列表框的指定位置插入一个串
BOOL OCombo::InsertString (int nIndex, char* pText)
{
	CHECK_TYPE_RETURN;

	return m_pList->InsertString (nIndex, pText);
}

// 删除列表框的所有列表项
BOOL OCombo::RemoveAll()
{
	CHECK_TYPE_RETURN;

	return m_pList->RemoveAll();
}

// 在列表框中查找一个串
int OCombo::FindString (char* pText)
{
	CHECK_TYPE_RETURN;

	return m_pList->FindString (pText);
}

// 根据列表框的内容更新编辑框
void OCombo::SelectString (char* pText)
{
	CHECK_TYPE;

	int iIndex = m_pList->SelectString(pText);
	if (iIndex != -1) {
		SetText (pText, WINDOW_CAPTION_BUFFER_LEN-1);
	}
}

// Edit得到焦点
void OCombo::EditSetFocus()
{
	CHECK_TYPE;

	m_pEdit->m_wStatus |= WND_STATUS_FOCUSED;

	O_MSG msg;
	msg.pWnd = m_pEdit;
	msg.message = OM_SETFOCUS;
	msg.wParam = 0;
	msg.lParam = 0;
	m_pApp->PostMsg (&msg);
}

// Edit失去焦点
void OCombo::EditKillFocus()
{
	CHECK_TYPE;

	m_pEdit->m_wStatus &= ~WND_STATUS_FOCUSED;
	O_MSG msg;
	msg.pWnd = m_pEdit;
	msg.message = OM_KILLFOCUS;
	msg.wParam = 0;
	msg.lParam = 0;
	m_pApp->SendMsg (&msg);
}

// List得到焦点
void OCombo::ListSetFocus()
{
	CHECK_TYPE;

	m_pList->m_wStatus |= WND_STATUS_FOCUSED;
	O_MSG msg;
	msg.pWnd = m_pList;
	msg.message = OM_SETFOCUS;
	msg.wParam = 0;
	msg.lParam = 0;
	m_pApp->PostMsg (&msg);
}

// List失去焦点
void OCombo::ListKillFocus()
{
	CHECK_TYPE;

	m_pList->m_wStatus &= ~WND_STATUS_FOCUSED;
	O_MSG msg;
	msg.pWnd = m_pList;
	msg.message = OM_KILLFOCUS;
	msg.wParam = 0;
	msg.lParam = 0;
	m_pApp->SendMsg (&msg);
}

// 将List当前的字符串拷贝到Edit中
void OCombo::SyncString()
{
	CHECK_TYPE;

	char sText [WINDOW_CAPTION_BUFFER_LEN];
	memset (sText, 0x0, WINDOW_CAPTION_BUFFER_LEN);
	int nIndex = m_pList->GetCurSel();
	if (nIndex != -1)
	{
		GetString (nIndex, sText);
		SetText (sText, WINDOW_CAPTION_BUFFER_LEN -1);
		
		// 如果本控件是父窗口的焦点，则设置Caret
		if (m_pParent != NULL)
		{
			OWindow* pParentActive = m_pParent->m_pActive;
			if (pParentActive == this)
				m_pEdit->SetSel (0, WINDOW_CAPTION_BUFFER_LEN -1);
		}
	}
}

// 判断是否可以向编辑框输入
BOOL OCombo::CanEdit()
{
	CHECK_TYPE_RETURN;

	if ((m_dwAddData1 & COMBO_DISABLE_EDIT) == 0) {
		return TRUE;
	}
	else {
		return FALSE;
	}
}

// 判断是否可以弹出列表框
BOOL OCombo::CanDropDown()
{
	CHECK_TYPE_RETURN;

	if ((m_dwAddData1 & COMBO_DISABLE_DROPDOWN) == 0) {
		return TRUE;
	}
	else {
		return FALSE;
	}
}

/* END */