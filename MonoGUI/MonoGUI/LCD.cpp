////////////////////////////////////////////////////////////////////////////////
// @file LCD.cpp
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#include <fstream>
using namespace std;

#if defined (RUN_ENVIRONMENT_LINUX)
#include <linux/fb.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#endif // defined(RUN_ENVIRONMENT_LINUX)

#include "MonoGUI.h"


BYTE* LCD::asc12 = NULL;
BYTE* LCD::hzk12 = NULL;

LCD::LCD()
{
#if defined (RUN_ENVIRONMENT_LINUX)
	m_nFB = 0;
#endif // defined(RUN_ENVIRONMENT_LINUX)

	fb_w = 0;
	fb_h = 0;
	fbmem = NULL;
	IsCited = FALSE;
}

LCD::LCD (int w, int h)
{
	int buffer_bytes = ((w + 7) / 8) * h;
	if (buffer_bytes > 0) {
		
		fb_w = w;
		fb_h = h;
		fbmem = new BYTE[buffer_bytes];
		memset (fbmem, 0xFF, buffer_bytes);
	}

	IsCited = FALSE;
}

LCD::LCD (LCD& other, BOOL bCopy)
{
	int W;
	int H;
	BYTE* buffer = other.GetBuffer(&W, &H);

	if (bCopy) {

		CopyBuffer (buffer, W, H);
		IsCited = FALSE;
	}
	else {

		fbmem = buffer;
		fb_w = W;
		fb_h = H;
		IsCited = TRUE;
	}
}

LCD::LCD (BW_IMAGE& bwimg, BOOL bCopy)
{
	if (bCopy) {

		CopyBuffer (bwimg.buffer, bwimg.w, bwimg.h);
		IsCited = FALSE;
	}
	else {

		fbmem = bwimg.buffer;
		fb_w = bwimg.w;
		fb_h = bwimg.h;
		IsCited = TRUE;
	}
}

LCD::LCD (BYTE* buffer, int w, int h, BOOL bCopy)
{
	if (bCopy) {

		CopyBuffer (buffer, w, h);
		IsCited = FALSE;
	}
	else {

		fbmem = buffer;
		fb_w = w;
		fb_h = h;
		IsCited = TRUE;
	}
}

LCD::~LCD()
{
#if defined (RUN_ENVIRONMENT_LINUX)
	if (m_nFB > 0) {
		ioctl (m_nFB, FBIOSTARTTIMER, 0);
		close (m_nFB);
		m_nFB = 0;
	}
#endif // defined(RUN_ENVIRONMENT_LINUX)

	ReleaseBuffer();
}

//黑白翻转
void LCD::Inverse (void)
{
	if (0 == fb_w ||
		0 == fb_h ||
		NULL == fbmem) {
			return;
	}

	int buffer_bytes = ((fb_w + 7) / 8) * fb_h;

	if (IsCited) // 引用内容，不要污染了原图
	{
		BYTE* tmp = new BYTE[buffer_bytes];
		memcpy (tmp, fbmem, buffer_bytes);
		fbmem = tmp;
		IsCited = FALSE;
	}

	int i;
	for (i = 0; i < buffer_bytes; i++) {
		fbmem[i] = ~(fbmem[i]);
	}
}

void LCD::ReleaseBuffer (void)
{
	if (fbmem != NULL
		&& ! IsCited) {
		delete [] fbmem;
	}
	fbmem = NULL;
	fb_w = 0;
	fb_h = 0;
}

#if defined (RUN_ENVIRONMENT_LINUX)
/* initilize the LCM, get the pointer of FrameBuffer */
BOOL LCD::LinuxInit (char* dev_file)
{
	struct	fb_var_screeninfo fbvar;
	struct	fb_fix_screeninfo fbfix;
	int	fd;

	if ((m_nFB = open(dev_file,O_WRONLY)) == -1)
	{
		DebugPrintf ("ERROR: open fb0 error\n");
		exit (-1);
	}

	if (ioctl(m_nFB,FBIOGET_VSCREENINFO,&fbvar))
	{
		DebugPrintf ("ERROR: can't get fb var info\n");
		exit (-1);
	}

	if (ioctl(m_nFB,FBIOGET_FSCREENINFO,&fbfix))
	{
		DebugPrintf ("ERROR: can't get fb fix info\n");
		exit (-1);
	}

	fb_w = fbvar.xres;
	fb_h = fbvar.yres;

	fd = open ("/dev/mem",O_RDWR);
	if (fd<=0)
	{
		DebugPrintf ("ERROR: can't open /dev/mem \n");
		exit (0);
	}
	fbmem = (BYTE *)mmap(0,fbfix.smem_len,PROT_READ|PROT_WRITE,MAP_SHARED,fd,fbfix.smem_start);
	close(fd);
	
	if (asc12 != NULL)
	{
		// The asc12 and hzk12 were only initialized once

		IsCopied = TRUE; // Tell the destructor do not delete the framebuffer

		ioctl (m_nFB, FBIOSTOPTIMER, 0);

		return TRUE;
	}

	asc12 = new BYTE [LENGTH_OF_ASC + 32];

#if defined (CHINESE_SUPPORT)
	hzk12 = new BYTE [LENGTH_OF_HZK + 32];
#endif // defined(CHINESE_SUPPORT)

	fd = open (ASC_FILE,O_RDONLY);
	if (fd == -1)
	{
		DebugPrintf ("ERROR: English font open error\n");
		delete [] asc12;
		asc12 = NULL;
		exit (0);
	}
	memset(asc12, 0x0, LENGTH_OF_ASC + 32);
	int iRlt = read(fd,asc12,LENGTH_OF_ASC);
	if(iRlt != LENGTH_OF_ASC)
	{
		DebugPrintf ("ERROR: English font read error\n");
	}
	close(fd);

#if defined (CHINESE_SUPPORT)
	DebugPrintf("hzkfile name:%s\n",HZK_FILE);
	fd = open (HZK_FILE,O_RDONLY);
	
	if (fd == -1)
	{
		DebugPrintf ("ERROR: Chinese font  open error\n");
		delete [] asc12;
		asc12 = NULL;
		delete [] hzk12;
		hzk12 = NULL;
		exit (0);
	}
	memset (hzk12,0x0,LENGTH_OF_HZK + 32);
	iRlt = read (fd,hzk12,LENGTH_OF_HZK); 
	if (iRlt != LENGTH_OF_HZK)
	{
		DebugPrintf ("ERROR: Chinese font read error\n");
	}
	close (fd);
#endif // defined(CHINESE_SUPPORT)

	IsCopied = TRUE; // Tell the destructor do not delete the framebuffer
	ioctl (m_nFB, FBIOSTOPTIMER, 0);
	return TRUE;
}

void LCD::LinuxFini (void)
{
	if (asc12 != NULL) {
		delete [] asc12;
		asc12 = NULL;
	}
	if (hzk12 != NULL) {
		delete [] hzk12;
		hzk12 = NULL;
	}
}

// for XWindows
BYTE* LCD::XWinGetFB(int* pw, int* ph)
{
	*pw = fb_w;
	*ph = fb_h;
	return fbmem;
}

// for XWindows
BOOL LCD::XWinInitVirtualLCD(int w, int h)
{
	if (fbmem) {
		return FALSE;
	}

	// 初始化图形缓冲区
	int buffer_bytes = ((w + 7) / 8) * h;
	if (buffer_bytes > 0) {
		fb_w = w;
		fb_h = h;
		fbmem = new BYTE[buffer_bytes];
		memset(fbmem, 0xFF, buffer_bytes);
	}

	IsCited = FALSE;

	// 初始化字库
	asc12 = new BYTE [LENGTH_OF_ASC + 32];

#if defined (CHINESE_SUPPORT)
	hzk12 = new BYTE [LENGTH_OF_HZK + 32];
#endif // defined(CHINESE_SUPPORT)

	int fd = open (ASC_FILE, O_RDONLY);
	if (fd == -1)
	{
		DebugPrintf ("ERROR: English font open error\n");
		delete [] asc12;
		asc12 = NULL;
		exit (0);
	}
	memset(asc12, 0x0, LENGTH_OF_ASC + 32);
	int iRlt = read(fd, asc12, LENGTH_OF_ASC);
	if (iRlt != LENGTH_OF_ASC) {
		DebugPrintf ("ERROR: English font read error\n");
	}
	close(fd);

#if defined (CHINESE_SUPPORT)
	DebugPrintf("hzkfile name:%s\n",HZK_FILE);
	fd = open (HZK_FILE,O_RDONLY);
	
	if (fd == -1) {
		DebugPrintf ("ERROR: Chinese font open error\n");
		delete [] asc12;
		asc12 = NULL;
		delete [] hzk12;
		hzk12 = NULL;
		exit (0);
	}
	memset (hzk12, 0x0, LENGTH_OF_HZK + 32);
	iRlt = read (fd, hzk12, LENGTH_OF_HZK); 
	if (iRlt != LENGTH_OF_HZK) {
		DebugPrintf ("ERROR: Chinese font read error\n");
	}
	close (fd);
#endif // defined(CHINESE_SUPPORT)

	return TRUE;
}
#endif // defined(RUN_ENVIRONMENT_LINUX)

#if defined (RUN_ENVIRONMENT_WIN32)
// 下面两个函数供Win32仿真环境使用
BOOL LCD::Win32Init (int w, int h)
{
	if (w < 1) {
		return FALSE;
	}
	if (h < 1) {
		return FALSE;
	}

	fb_w = w;
	fb_h = h;
	int iFrameBufferSize = (w * h) / 8;
	
	fbmem = new BYTE [iFrameBufferSize + 2];
	memset (fbmem, 0xFF, iFrameBufferSize + 2);

	if (asc12 != NULL)
	{
		// The asc12 and hzk12 were only initialized once
		IsCited = FALSE;  // Tell the destructor delete the framebuffer
		return TRUE;
	}

	asc12 = new BYTE [LENGTH_OF_ASC + 32];

#if defined (CHINESE_SUPPORT)
	hzk12 = new BYTE [LENGTH_OF_HZK + 32];
#endif // defined(CHINESE_SUPPORT)

	// 打开字库
	ifstream ascFile(ASC_FILE, ios::in | ios::binary);
	if (!ascFile.is_open()) {
		goto release;
	}

	memset(asc12, 0x0, LENGTH_OF_ASC + 32);
	ascFile.read((char *)asc12, LENGTH_OF_ASC);
	ascFile.close();


#if defined (CHINESE_SUPPORT)
	{
		ifstream hzkFile(HZK_FILE, ios::in | ios::binary);
		if (!hzkFile.is_open()) {
			goto release;
		}

		memset(hzk12, 0x0, LENGTH_OF_HZK + 32);
		hzkFile.read((char*)hzk12, LENGTH_OF_HZK);
		hzkFile.close();
	}
#endif // defined(CHINESE_SUPPORT)

	IsCited = FALSE;  // Tell the destructor delete the framebuffer
	return TRUE;

release:
	delete [] asc12;
	asc12 = NULL;

#if defined(CHINESE_SUPPORT)
	delete [] hzk12;
	hzk12 = NULL;
#endif // defined(CHINESE_SUPPORT)

	return FALSE;
}

void LCD::Win32Fini (void)
{
	if (asc12 != NULL) {
		delete [] asc12;
		asc12 = NULL;
	}
	if (hzk12 != NULL) {
		delete [] hzk12;
		hzk12 = NULL;
	}
}

void LCD::DebugSaveSnapshoot(char* bmp_filename)
{
	BITMAPFILEHEADER bmfHdr; //位图文件头结构
	BITMAPINFOHEADER bi;     //位图信息头结构

	if (0 == fb_w ||
		0 == fb_h ||
		NULL == fbmem) {
		return;
	}

	//设置位图信息头结构
	bi.biSize = sizeof(BITMAPINFOHEADER);  //本结构所占用字节数
	bi.biWidth = fb_w;          //位图的宽度，以像素为单位
	bi.biHeight = fb_h;         //位图的高度，以像素为单位
	bi.biPlanes = 1;            //目标设备的级别，必须为1
	bi.biBitCount = 1;          //每个像素所需的位数，1表示双色
	bi.biCompression = 0;       //位图压缩类型，0表示不压缩
	bi.biSizeImage = 0;         //位图的大小，以字节为单位
	bi.biXPelsPerMeter = 0;     //位图水平分辨率，每米像素数
	bi.biYPelsPerMeter = 0;     //位图垂直分辨率，每米像素数
	bi.biClrUsed = 0;           //位图实际使用的颜色表中的颜色数
	bi.biClrImportant = 0;      //位图显示过程中重要的颜色数

	//黑白位图的调色板
	RGBQUAD Palette[2] = { 0xFF, 0xFF, 0xFF, 0x0, 0,0,0,0 };

	//每一行数据的字节数必须是4字节的整数倍
	DWORD dwByteWidth = ((fb_w + 31) / 32) * 4;
	DWORD dwInputByteWidth = ((fb_w + 7) / 8);
	DWORD dwByteSize = dwByteWidth * fb_h;

	//重新排列位图数据
	BYTE* Dib = new BYTE[dwByteSize];
	memset(Dib, 0xFF, dwByteSize); //默认值为白色

	DWORD x8;
	int y;
	for (y = 0; y < fb_h; y++) {

		BYTE* line = Dib + dwByteWidth * y;
		BYTE* orig = fbmem + dwInputByteWidth * (fb_h - y - 1);
		for (x8 = 0; x8 < dwInputByteWidth; x8++) {

			line[x8] = orig[x8];
		}
	}

	// 创建位图文件
	ofstream bmpFile(bmp_filename, ios::out | ios::binary);
	if (!bmpFile.is_open()) {
		return;
	}
    
    // 设置位图文件头
    bmfHdr.bfType = 0x4D42; //位图文件的类型，必须为"BM"
    bmfHdr.bfSize =         //位图文件的大小，以字节为单位
		  sizeof (BITMAPFILEHEADER)
        + sizeof (BITMAPINFOHEADER)
        + sizeof (Palette)
		+ dwByteSize;
    bmfHdr.bfReserved1 = 0; //位图文件保留字，必须为0
    bmfHdr.bfReserved2 = 0; //位图文件保留字，必须为0
    bmfHdr.bfOffBits =      //位图数据的起始位置，相对于位图文件头的偏移量
		  sizeof (BITMAPFILEHEADER)
        + sizeof (BITMAPINFOHEADER)
        + sizeof (Palette);
    
    // 写入文件
	bmpFile.write((char *)&bmfHdr, sizeof(BITMAPFILEHEADER));
	bmpFile.write((char *)&bi, sizeof(BITMAPINFOHEADER));
	bmpFile.write((char *)&Palette, sizeof(Palette));
	bmpFile.write((char *)Dib, dwByteSize);
	bmpFile.close();

	delete [] Dib;
}
#endif // defined(RUN_ENVIRONMENT_WIN32)

#if defined (RUN_ENVIRONMENT_LINUX)
void LCD::Show (void)
{
	if (m_nFB > 0) {
		DebugPrintf("Debug: LCD::Show m_nFB > 0\n");
		ioctl (m_nFB, FBIOREFRESH, 0);
	}
}
#endif // defined(RUN_ENVIRONMENT_LINUX)

#if defined (RUN_ENVIRONMENT_WIN32)
void LCD::Show (HWND hWnd, BOOL bReverseMode)
{
	if (hWnd == NULL) {
		return;
	}

	int nBufferSize = (fb_w + 7) / 8 * fb_h;
	BYTE* pTemp = new BYTE [nBufferSize];

	if (bReverseMode) {
		// 屏幕颜色翻转
		int i;
		for (i = 0; i < nBufferSize; i++) {
			pTemp[i] = ~(fbmem[i]);
		}
	}
	else {
		memcpy (pTemp, fbmem, nBufferSize);
	}

	HBITMAP hBitmap = ::CreateBitmap (fb_w, fb_h, 1, 1, pTemp);

	HDC hdc = ::GetDC (hWnd);
	HDC hMemDC = ::CreateCompatibleDC (hdc);
	::DeleteObject (::SelectObject(hMemDC, hBitmap));

	if (SCREEN_MODE > 1) {
		StretchBlt (hdc,0,0,
			fb_w * SCREEN_MODE, fb_h * SCREEN_MODE,
			hMemDC,0,0,fb_w,fb_h,SRCCOPY);
	}
	else {
		::BitBlt (hdc, 0, 0, fb_w, fb_h, hMemDC, 0, 0, SRCCOPY);
	}

	::DeleteObject (hBitmap);
	::DeleteObject (hMemDC);
	::ReleaseDC (hWnd, hdc);

	delete [] pTemp;
}
#endif // defined(RUN_ENVIRONMENT_WIN32)


/* get the pointer of FrameBuffer, and the width, the height */
BYTE* LCD::GetBuffer (int* pnW, int* pnH)
{
	*pnW = fb_w;
	*pnH = fb_h;
	return fbmem;
}

/* copy framebuffer data from another buffer */
void LCD::CopyBuffer (BYTE* buffer, int w, int h)
{
	ReleaseBuffer();

	int buffer_bytes = ((w + 7) / 8) * h;
	if (buffer_bytes > 0) {
			
		fbmem = new BYTE[buffer_bytes];
		memcpy (fbmem, buffer, buffer_bytes);
	}

	IsCited = FALSE;
}

/* set a pixel color of the identified position */
void LCD::SetPixel (int x, int y, int color)
{
    if (x < 0 || x >= fb_w) return;
    if (y < 0 || y >= fb_h) return;

    DWORD location = ((y * fb_w) / 8) + (x / 8);
    BYTE abyte = *(fbmem + location);
    if (color) {
		*(fbmem + location) |= mask_set_bit [x & 7];
	}
    else {
		*(fbmem + location) &= mask_clr_bit [x & 7];
	}
}

/* get the color of the special pixel */
int LCD::GetPixel (int x, int y)
{
	return BWImgGetPixel (fbmem, fb_w, fb_h, x, y);
}

/* draw a vertical line on the buffer */
void LCD::VLine (int x, int y, int l, int color)
{
	if (x >= fb_w) return;
	if (y >= fb_h) return;
	if (l <= 0)    return;
	if (x <  0)    return;

	/* trim parameters according to the framebuffer size */
	if (y < 0) {
		l = (l + y);
		y = 0;
	}
    if ((l + y) > fb_h) {
        l = fb_h - y;
	}
	// 

    int i;
    DWORD location = ((y * fb_w) / 8) + (x / 8);

    int buf_w_byte = fb_w / 8;
    
    if (color)
    {
        for (i=0; i<l; i++)
        {
			*(fbmem + location) |= mask_set_bit [x & 7];
			location += buf_w_byte;
		}
    }
    else
    {
		for (i=0; i<l; i++)
		{
			*(fbmem + location) &= mask_clr_bit [x & 7];
			location += buf_w_byte;
		}
    }
}

/* draw a horizontal line on the buffer */
void LCD::HLine (int x, int y, int l, int color)
{
	if (x >= fb_w) return;
	if (y >= fb_h) return;
	if (l <= 0)    return;
	if (y <  0)    return;

	// 根据屏幕大小对绘制参数进行剪裁
	if (x < 0) {
		l = (l + x);
		x = 0;
	}
    if ((l + x) > fb_w) {
        l = fb_w - x;
	}
	//

    BYTE mask;
    int i;

    int start = ((y * fb_w) / 8) + (x / 8);
    int end = ((y * fb_w) / 8) + ((x + l) / 8);
    int offset_start = x & 7;
    int offset_end = (x + l) & 7;
    
    if (color)
    {
		if (start == end)	/* the line is in one byte */
		{
			mask  = mask_byte [offset_start + 8];
			mask &= mask_byte [offset_end];
			*(fbmem + start) |= mask;
		}
		else
		{
			mask  = mask_byte [offset_start + 8];
			*(fbmem + start) |= mask;		/* draw the first byte */

			for (i=1; i<(end-start); i++)	/* draw the middle bytes */
			{
				*(fbmem + start + i) = 0xff;
			}

			if (offset_end != 0)			/* draw the last byte */
			{
				mask = mask_byte [offset_end];
				*(fbmem + end) |= mask;
			}
		}
    }
    else
    {
		if (start == end)	/* the line is located in one byte */
		{
			mask  = mask_byte [offset_start];
			mask |= mask_byte [offset_end + 8];
			*(fbmem + start) &= mask;
		}
		else
		{
			mask = mask_byte [offset_start];
			*(fbmem + start) &= mask;		/* draw the first byte */

			for (i=1; i<(end-start); i++)	/* draw the middle bytes */
			{
				*(fbmem + start + i) = 0x0;
			}

			if (offset_end != 0)			/* draw the last byte */
			{
	    		mask  = mask_byte [offset_end + 8];
	    		*(fbmem + end) &= mask;
			}
		}
    }
}

/* fill an rect area with identified color */
void LCD::FillRect (int x, int y, int w, int h, int color)
{
	if (x >= fb_w) return;
	if (y >= fb_h) return;
	if (w <= 0)    return;
	if (h <= 0)    return;

	// 根据屏幕大小对绘制参数进行剪裁
	if (x < 0) {
		w = (w + x);
		x = 0;
	}
    if ((w + x) > fb_w) {
        w = fb_w - x;
	}

	if (y < 0) {
		h = (h + y);
		y = 0;
	}
	if ((h + y) > fb_h) {
		h = fb_h - y;
	}
	//

    int i;
    if (color != 2)
    {
    	for (i=0; i<h; i++) {
	        HLine (x, y+i, w, color);
		}
    }
    else	// black and white
    {
		int j;
		int cur_x = 0;
		int cur_y = 0;
		int cur_color = 0;
		
		for (i=0; i<h; i++)
		{
			for (j=0; j<w; j++)
			{
				cur_x = x + j;
				cur_y = y + i;
				if ((cur_x + cur_y) % 2 == 1)
				{
					cur_color = 1;
				}
				else
				{
					cur_color = 0;
				}
    			SetPixel (cur_x, cur_y, cur_color);
			}
		}
    }
}

/* draw a rectangle image with identified mode */
/* image matrix: 0: clear a bit; 1: set a bit; 2: keep background color; 3: inverse background color */
void LCD::DrawImage (int x, int y, int w, int h, FOUR_COLOR_IMAGE& img, int sx, int sy, int mode)
{
    if (w <= 0) return;
    if (h <= 0) return;
    if (x > fb_w) return;
    if (y > fb_h) return;
    
    int width  = w;
    if ((x+width) > fb_w) {
		width = fb_w - x;
	}
	if ((sx+width) > img.w) {
		width = img.w - sx;
	}

    int height = h;
    if ((y+height) > fb_h) {
		height = fb_h - y;
	}
	if ((sy+height) > img.h) {
		height = img.h - sy;
	}
    
    int i;
    int j;
    for (i=0; i<width; i++)
    {
        for (j=0; j<height; j++)
        {
			char cr = img.buffer[(sy + j) * img.w + sx + i];

			if (cr == FOUR_COLOR_TRANSPARENT) {
				continue;
			}

			if (cr == FOUR_COLOR_INVERSE)
			{
				char c = GetPixel (x+i, y+j);
				c = (c==0 ? 1 : 0);
				SetPixel (x+i, y+j, c);
				continue;
			}
			
			if (mode == LCD_MODE_INVERSE)
			{
				cr = (cr==0 ? 1 : 0);
			}
			else if (mode == LCD_MODE_OR)
			{
				if (cr == 0) {
					continue;
				}
			}
			else if (mode == LCD_MODE_AND)
			{
				if (cr == 1) {
					continue;
				}
			}
			else if (mode == LCD_MODE_NOR)
			{
				char c = GetPixel (x+i, y+j);
				cr ^= c;
			}
			else if (mode == LCD_MODE_BLACKNESS)
			{
				cr = 0;
			}
			else if (mode == LCD_MODE_WHITENESS)
			{
				cr = 1;
			}

			SetPixel (x+i, y+j, cr);
		}
    }
}

/* draw a black-and-white image with identified mode */
void LCD::DrawImage (int x, int y, int w, int h, BW_IMAGE& bwimg, int sx, int sy, int mode)
{
    if (w <= 0)   return;
    if (h <= 0)   return;
    if (x > fb_w) return;
    if (y > fb_h) return;
    
    int width = w;
    if ((x+width) > fb_w) {
		width = fb_w - x;
	}
    if ((sx+width) > bwimg.w) {
		width = bwimg.w - sx;
	}
    
    int height = h;
    if ((y+height) > fb_h) {
		height = fb_h - y;
	}
    if ((sy+height) > bwimg.h) {
		height = bwimg.h - sy;
	}
    
    int i;
    int j;
    for (i=0; i<width; i++)
    {
        for (j=0; j<height; j++)
        {
			int c = BWImgGetPixel (bwimg.buffer, 
				bwimg.w, bwimg.h, sx+i, sy+j);

			switch (mode)
			{
			case LCD_MODE_NORMAL:		/* normal */
				SetPixel (x+i, y+j, c);
				break;

			case LCD_MODE_INVERSE:		/* inverse */
				c = (c==0 ? 1 : 0);
				SetPixel (x+i, y+j, c);
				break;

			case LCD_MODE_OR:		/* or mode */
				if (c == 1)
				{
					SetPixel (x+i, y+j, c);
				}
				break;

			case LCD_MODE_AND:		/* and mode */
				if (c == 0)
				{
					SetPixel (x+i, y+j, c);
				}
				break;

			case LCD_MODE_NOR:		/* nor mode */
				if (c == 1)
				{
					BYTE c2 = GetPixel (x+i, y+j);
					c2 = (c2==0 ? 1 : 0);
					SetPixel (x+i, y+j, c2);
				}
				break;

			case LCD_MODE_BLACKNESS:
				SetPixel (x+i, y+j, 0);
				break;

			case LCD_MODE_WHITENESS:
				SetPixel (x+i, y+j, 1);
				break;

			default:
				{
					DebugPrintf("ERR: invalid parameter: BitBlt function, para 5, mode. ");
				}
			}
		}
    }
}

/* draw an identified string */
void LCD::TextOut (int x, int y, BYTE* text, int l, int mode)
{
    if (l == 0) return;
    if (x >= fb_w) return;
    if (y >= fb_h) return;
    
    int length = l;
    if (length == -1) {
		length = DRAW_TEXT_LENGTH_MAX;
	}
    
    int current_x = x;
    int current_y = y;
    int current = 0;
    BYTE cHiByte;


#if defined (CHINESE_SUPPORT)
    BYTE cLoByte;
#endif // defined(CHINESE_SUPPORT)
    
    while (current < length)
    {
    	cHiByte = *(text + current);
		current ++;
		if (cHiByte == 0) return;    /* found the END character */

		if (cHiByte < 128)  /* an ENGLISH character */
		{
			DWORD offset = cHiByte * ASC_H;
			if (offset > LENGTH_OF_ASC) continue;
			
			LCD ch ((asc12 + offset), 8, ASC_H, FALSE);
			// 字库中bit1表示黑(0)，bit0表示白(1)
			InverseBlt (current_x, current_y, ASC_W+ASC_GAP, ASC_H, ch, 0, 0, mode);
			current_x += (ASC_W + ASC_GAP);
		}
		else               /* a CHINESE character */
		{

#if defined (CHINESE_SUPPORT)

			cLoByte = *(text + current);
			current ++;
			if (cLoByte == 0) return;  /* found the END character */
			
			DWORD offset = GetHzIndex (cHiByte, cLoByte, (HZK_H * 2));
			if (offset < 0) continue;
			if (offset > LENGTH_OF_HZK) continue;

			LCD ch ((hzk12 + offset), 16, HZK_H, FALSE);
			// 字库中bit1表示黑(0)，bit0表示白(1)
			InverseBlt (current_x, current_y, HZK_W+HZK_GAP, HZK_H, ch, 0, 0, mode);
			current_x += (HZK_W + HZK_GAP);

#else
			return;
#endif // defined(CHINESE_SUPPORT)

		}
    }
}

/* draw an string which snap ASC characters to CHINESE characters. */
void LCD::TextOut_Align (int x, int y, BYTE* text, int l, int mode)
{
    if (l == 0) return;
    if (x >= fb_w) return;
    if (y >= fb_h) return;
    
    int length = l;
    if (length == -1) {
		length = DRAW_TEXT_LENGTH_MAX;
	}
    
    int current_x = x;
    int current_y = y;
    int current = 0;
    BYTE cHiByte;

#if defined (CHINESE_SUPPORT)
    BYTE cLoByte;
#endif // defined(CHINESE_SUPPORT)
       
    while (current < length)
    {
    	cHiByte = *(text + current);
		current ++;
		if (cHiByte == 0) return;		/* found the END character */

		if (cHiByte < 128)			/* an ENGLISH character */
		{
			DWORD offset = cHiByte * ASC_H;
			if (offset > LENGTH_OF_ASC) continue;
			
			LCD ch ((asc12 + offset), 8, ASC_H, FALSE);
			// 字库中bit1表示黑(0)，bit0表示白(1)
			InverseBlt (current_x, current_y, ASC_W+ASC_GAP, ASC_H, ch, 0, 0, mode);
			current_x += (ASC_W + ASC_GAP);
		}
		else						/* a CHINESE character */
		{

#if defined (CHINESE_SUPPORT)
			cLoByte = *(text + current);
			current ++;
			if (cLoByte == 0) return;		/* found the END character */
			
			DWORD offset = GetHzIndex (cHiByte, cLoByte, (HZK_H * 2));
			if (offset < 0) continue;
			if (offset > LENGTH_OF_HZK) continue;

			LCD ch ((hzk12 + offset), 16, HZK_H, FALSE);
			// 字库中bit1表示黑(0)，bit0表示白(1)
			InverseBlt (current_x, current_y, HZK_W+HZK_GAP, HZK_H, ch, 0, 0, mode);
			current_x += (ASC_W + ASC_GAP) * 2;
#else
			return;
#endif // defined(CHINESE_SUPPORT)

		}
    }
}

/* copy context from one FrameBuffer-Compatible buffer to another */
void LCD::Copy (LCD& other, int mode)
{
	int sW;
	int sH;
	BYTE* source_buffer = other.GetBuffer(&sW, &sH);
    if ((source_buffer == fbmem) && (mode == COPY_MODE_NORMAL)) {
		return;
	}

	if (source_buffer == NULL
		|| sW == 0
		|| sH == 0) {

		return;
	}

	if (fb_w != sW ||
		fb_h != sH) {

		return;
	}

	int buffer_bytes = ((fb_w + 7) / 8) * fb_h;

    if (mode == COPY_MODE_NORMAL)
    {
		memcpy (fbmem, source_buffer, buffer_bytes);
    }
    else if (mode == COPY_MODE_INVERSE)
    {
		int i;
		for (i = 0; i < buffer_bytes; i++){
			*(fbmem + i) = ~ *(source_buffer + i);	/* inverse each byte */
		}
    }
    else if (mode == COPY_MODE_BLACKNESS)
    {
		memset (fbmem, 0x0, buffer_bytes);
    }
    else if (mode == COPY_MODE_WHITENESS)
    {
		memset (fbmem, 0xFF, buffer_bytes);
    }
}

void LCD::BitBlt (int x, int y, int w, int h, LCD& other, int sx, int sy, int mode)
{
    if (w <= 0)   return;
    if (h <= 0)   return;
    if (x > fb_w) return;
    if (y > fb_h) return;
    
    int width = w;
    if ((x+width) > fb_w) {
		width = fb_w - x;
	}
    if ((sx+width) > other.fb_w) {
		width = other.fb_w - sx;
	}
    
    int height = h;
    if ((y+height) > fb_h) {
		height = fb_h - y;
	}
    if ((sy+height) > other.fb_h) {
		height = other.fb_h - sy;
	}
    
    int i;
    int j;
    for (i=0; i<width; i++)
    {
        for (j=0; j<height; j++)
        {
			int c = other.GetPixel (sx+i, sy+j);
			switch (mode)
			{
			case LCD_MODE_NORMAL:		/* normal */
				SetPixel (x+i, y+j, c);
				break;

			case LCD_MODE_INVERSE:		/* inverse */
				c = (c==0 ? 1 : 0);
				SetPixel (x+i, y+j, c);
				break;

			case LCD_MODE_OR:		/* or mode */
				if (c == 1)
				{
					SetPixel (x+i, y+j, c);
				}
				break;

			case LCD_MODE_AND:		/* and mode */
				if (c == 0)
				{
					SetPixel (x+i, y+j, c);
				}
				break;

			case LCD_MODE_NOR:		/* nor mode */
				if (c == 1)
				{
					BYTE c2 = GetPixel (x+i, y+j);
					c2 = (c2==0 ? 1 : 0);
					SetPixel (x+i, y+j, c2);
				}
				break;

			default:
				{
					DebugPrintf("ERR: invalid parameter: BitBlt function, para 5, mode. ");
				}
			}
		}
    }
}

// 模式不变，黑色与白色互换
void LCD::InverseBlt (int x, int y, int w, int h, LCD& other, int sx, int sy, int mode)
{
    if (w <= 0)   return;
    if (h <= 0)   return;
    if (x > fb_w) return;
    if (y > fb_h) return;
    
    int width = w;
    if ((x+width) > fb_w) {
		width = fb_w - x;
	}
    if ((sx+width) > other.fb_w) {
		width = other.fb_w - sx;
	}
    
    int height = h;
    if ((y+height) > fb_h) {
		height = fb_h - y;
	}
    if ((sy+height) > other.fb_h) {
		height = other.fb_h - sy;
	}
    
    int i;
    int j;
    for (i=0; i<width; i++)
    {
        for (j=0; j<height; j++)
        {
			int c = other.GetPixel (sx+i, sy+j);
			c = (c == 0) ? 1 : 0;

			switch (mode)
			{
			case LCD_MODE_NORMAL:		/* normal */
				SetPixel (x+i, y+j, c);
				break;

			case LCD_MODE_INVERSE:		/* inverse */
				c = (c==0 ? 1 : 0);
				SetPixel (x+i, y+j, c);
				break;

			case LCD_MODE_OR:		/* or mode */
				if (c == 1)
				{
					SetPixel (x+i, y+j, c);
				}
				break;

			case LCD_MODE_AND:		/* and mode */
				if (c == 0)
				{
					SetPixel (x+i, y+j, c);
				}
				break;

			case LCD_MODE_NOR:		/* nor mode */
				if (c == 1)
				{
					BYTE c2 = GetPixel (x+i, y+j);
					c2 = (c2==0 ? 1 : 0);
					SetPixel (x+i, y+j, c2);
				}
				break;

			default:
				{
					DebugPrintf("ERR: invalid parameter: BitBlt function, para 5, mode. ");
				}
			}
		}
    }
}

void LCD::Line (int x1, int y1, int x2, int y2, int color)
{
	// 如果是垂直线，则直接调用绘制竖线的函数
	if (x1 == x2)
	{
		int y = (y1 < y2)? y1 : y2;

		int dy = y1 - y2;
		if (dy < 0) {
			dy = -dy;
		}

		VLine (x1, y, (dy+1), color);
		return;
	}

	// 如果是水平直线，则直接调用绘制横线的函数
	if (y1 == y2)
	{
		int x = (x1 < x2)? x1 : x2;

		int dx = x1 - x2;
		if (dx < 0) {
			dx = -dx;
		}

		HLine (x, y1, (dx+1), color);
		return;
	}

	int dx = x1 - x2;
	if (dx < 0) {
		dx = -dx;
	}

	int dy = y1 - y2;
	if (dy < 0) {
		dy = -dy;
	}

	if (dx > dy)
	{
		// 交换坐标，使X1Y1成为左边的点
		if (x1 > x2)
		{
			int x = x1;
			int y = y1;
			x1    = x2;
			x2    = x;
			y1    = y2;
			y2    = y;
		}

		if (y1 < y2)
		{
			int offset = 0;
			int i;
			for (i=0; i<dx; i++)
			{
				SetPixel (x1, y1, color);
				offset += dy;
				if (offset>dx)
				{
					offset -= dx;
					y1++;
				}
				x1++;
			}
		}
		else
		{
			int offset = 0;
			int i;
			for (i=0; i<dx; i++)
			{
				SetPixel (x1, y1, color);
				offset += dy;
				if (offset>dx)
				{
					offset -= dx;
					y1--;
				}
				x1++;
			}
		}
	}
	else
	{
		// 交换坐标，使X1Y1成为上的点
		if (y1 > y2)
		{
			int x = x1;
			int y = y1;
			x1    = x2;
			x2    = x;
			y1    = y2;
			y2    = y;
		}

		if (x1 < x2)
		{
			int offset = 0;
			int i;
			for( i=0; i<dy; i++ )
			{
				SetPixel (x1, y1, color);
				offset += dx;
				if (offset>dy)
				{
					offset -= dy;
					x1++;
				}
				y1++;
			}
		}
		else
		{
			int offset = 0;
			int i;
			for (i=0; i<dy; i++)
			{
				SetPixel (x1, y1, color);
				offset += dx;
				if (offset>dy)
				{
					offset -= dy;
					x1--;
				}
				y1++;
			}
		}
	}
}

/* end */