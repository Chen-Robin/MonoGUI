////////////////////////////////////////////////////////////////////////////////
// @file OIMEDB.h
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#if !defined(__OIME_DB_H__)
#define __OIME_DB_H__

// 定义各数字键对应的英文字母
const BYTE Key0[4] = {'\0','\0','\0','\0'};
const BYTE Key1[4] = {'\0','\0','\0','\0'};
const BYTE Key2[4] = { 'a', 'b', 'c','\0'};
const BYTE Key3[4] = { 'd', 'e', 'f','\0'};
const BYTE Key4[4] = { 'g', 'h', 'i','\0'};
const BYTE Key5[4] = { 'j', 'k', 'l','\0'};
const BYTE Key6[4] = { 'm', 'n', 'o','\0'};
const BYTE Key7[4] = { 'p', 'q', 'r', 's'};
const BYTE Key8[4] = { 't', 'u', 'v','\0'};
const BYTE Key9[4] = { 'w', 'x', 'y', 'z'};

// 输入法索引表的一个单元
#pragma pack(2)
typedef struct _SY_INDEX_ITEM
{
	BYTE  sSymbol[6];
	WORD  wLength;
	DWORD dwStart;
} SY_INDEX_ITEM;
//sizeof(SY_INDEX_ITEM)--->12
#pragma pack()

// 联想词汇索引表的一个单元
#pragma pack(2)
typedef struct _HZ_INDEX_ITEM
{
	BYTE  sCharacter[2];
	WORD  wLength;
	DWORD dwStart;
} HZ_INDEX_ITEM;
//sizeof(HZ_INDEX_ITEM)--->8
#pragma pack()


class OIME_DB
{
private:
	int m_nCurIME;				// 当前输入法：1标准拼音；2方言扩展

	char m_cSyLength;			// 拼音输入表已经输入的字符数目
	char m_sSymbol [7];			// 拼音的输入表，保存输入的原始字符(数字)

	BYTE* m_pPyLib;				// 标准拼音输入法库
	int m_nPyCount;				// 标准拼音输入法的条目数
	SY_INDEX_ITEM* m_pPyTable;	// 标准拼音输入法索引表

	BYTE* m_pExLib;				// 方言扩展输入法库
	int m_nExCount;				// 方言扩展输入法的条目数
	SY_INDEX_ITEM* m_pExTable;	// 方言扩展输入法索引表

	BYTE* m_pLxLib;				// 联想词库
	int m_nLxCount;				// 联想词库的条目数
	HZ_INDEX_ITEM* m_pLxTable;	// 联想词库索引表

	int m_nSyCount;				// 拼音返回集中单元的数目
	BYTE* m_pSy;				// 拼音查询的返回集(7个字节一组，每组第一个字节是状态字)
	
	int m_nHzCount;				// 汉字返回集的长度
	BYTE* m_pHzTable;			// 输入法的汉字字符串

	int m_nLxHzCount;			// 联想词汇返回集的长度
	BYTE* m_pLxHzTable;			// 联想的汉字字符串

public:
	OIME_DB ();
	virtual ~OIME_DB ();

	// 释放数据表
	void ReleaseDataTable();

	// 检查数据表文件是否已经存在
	BOOL CheckDataTable();

	// 初始化输入法库
	// 该函数仅提供Win32版本，目标系统不需要调用此函数
	BOOL CreateDataTable();

	// 从文件加载数据表
	BOOL LoadDataTable();

	// 设定当前输入法：1标准拼音；2方言扩展
	int SetCurIME (int nIME);

	// 向输入表添加一个字符
	BOOL SyAdd (char ch);

	// 删除输入表的最后一个字符
	BOOL SyRemove ();

	// 清空输入表
	BOOL SyRemoveAll ();

	// 得到输入表的长度
	int GetSyLength ();

	// 根据拼音输入表查找匹配的拼音
	int GetSy (BYTE** pSy);

	// 根据拼音查找汉字
	int GetHz (BYTE* pInput, BYTE** pResult);

	// 根据汉字查找联想字
	int GetLx (BYTE* pInput, BYTE** pResult);

private:
	// 得到数字键所对应的4个英文字母
	void GetKeySymbol (char* pKey, char cNum);

	// 二分搜索法的搜索子程序
	// iStart:开始位置；iEnd:结束位置；pIndex:索引字符串；pString:参加比较的字符串
	// 用拼音搜索汉字
	int SearchHz (int nStart, int nEnd, SY_INDEX_ITEM* pIndex, BYTE* pString);

	// 用汉字搜索联想汉字
	int SearchLx (int nStart, int nEnd, HZ_INDEX_ITEM* pIndex, BYTE* pString);
};

#endif // !defined(__OIME_DB_H__)
