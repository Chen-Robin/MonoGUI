////////////////////////////////////////////////////////////////////////////////
// @file OTimerQueue.h
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#if !defined(__OTIMERQUEUE_H__)
#define __OTIMERQUEUE_H__


typedef struct _O_TIMER
{
	OWindow*    pWnd;           // 设定定时器的窗口
	int         ID;             // 定时器序号

	ULONGLONG   lasttime;       // 上次时间

	int         interval;       // 时间间隔（单位：毫秒）
} O_TIMER;

#define TIMER_QUEUE_SIZE    TIMER_MAX

class OTimerQueue
{
private:
	int m_nCount;
	O_TIMER m_arTimerQ[TIMER_QUEUE_SIZE];

public:
	OTimerQueue();
	virtual ~OTimerQueue();

	// 添加一个定时器；
	// 如果当前定时器的数量已经达到TIMER_MAX所定义的数目，则返回FALSE；
	// 如果发现一个ID与当前定时器相同的定时器，则直接修改该定时器的设定；
	BOOL SetTimer (OWindow* pWindow, int nTimerID, int interval);

	// 删除一个定时器；
	// 根据TimerID删除
	BOOL KillTimer (int nTimerID);

	// 检查定时器队列；
	// 如果发现某个定时器到时了，首先用FindMsg函数检查消息队列中有没有同一个定时器发出的消息，
	// 如果没有，则使用PostMsg函数向消息队列中插入MSG_TIMER消息；
	// 如果PostMsg向消息队列插入消息失败，则该函数返回FALSE；
	BOOL CheckTimer (OApp* pApp);

private:
	BOOL RemoveAll ();
};

#endif // !defined(__OTIMERQUEUE_H__)
