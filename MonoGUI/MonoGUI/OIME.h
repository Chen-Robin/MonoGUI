////////////////////////////////////////////////////////////////////////////////
// @file OIME.h
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#if !defined (__OIME_H__)
#define __OIME_H__

//m_nCurIME对应的输入法选项
enum
{
	IME_OFF = 0,   //未打开输入法
	IME_PY  = 1,   //全键拼音
	IME_FY  = 2,   //全键方言
	IME_9PY = 3,   //九键拼音
	IME_9FY = 4,   //九键方言
	IME_9LE = 5,   //九键英文小写
	IME_9UE = 6,   //九键英文大写
	IME_SY  = 7,   //九键标点符号
	IME_END = 8
};
#define IME_BEGIN   IME_PY

// 定义各种符号
#define IME_PUNCTUATION_NUM 33		// 有33个字符
const BYTE strPunctuation[] =
{
' ', 0x20,  '!', 0x20, '\"', 0x20,  '#', 0x20,  '$', 0x20,  '%', 0x20,  '&', 0x20, '\'', 0x20,  '(', 0x20,
')', 0x20,  '*', 0x20,  '+', 0x20,  ',', 0x20,  '-', 0x20,  '.', 0x20,  '/', 0x20,  ':', 0x20,  ';', 0x20,
'<', 0x20,  '=', 0x20,  '>', 0x20,  '?', 0x20,  '@', 0x20,  '[', 0x20, '\\', 0x20,  ']', 0x20,  '^', 0x20,
'_', 0x20,  '`', 0x20,  '{', 0x20,  '|', 0x20,  '}', 0x20,  '~', 0x20
};

#define IME_TIMER_ID			100	// 输入法窗口使用的定时器ID值
#define IME_KEY_PRESS_GAP		2000	// 英文输入法中按键间隔的最大秒数
// 注：如果两次按下同一个键不超过这个时间，则切换字符，超过则输入新的字符
#define IME_CURRENT_UPPER		0	// 当前输入为上栏
#define IME_CURRENT_LOWER		1	// 当前输入为下栏
#define IME_OUT_STRING_LIMIT	32	// 英文输入法中输入字符串的最大长度


class OIME : public OWindow	// (从OWindow派生为了能够接收消息)
{
private:
	enum { self_type = WND_TYPE_IME };

public:
	int m_nCurIME;				// 当前输入法：
	int m_nCurInputLine;		// 当前输入行：0:上边的拼音行；1:下边的汉字行。

private:
	OWindow* m_pTargetWnd;		// 输出汉字的目标窗口
	OIME_DB* m_pIMEDB;			// 输入法数据库

	int m_nOutStrLength;		// 有效输出字符串的长度(只用于英文输入法)

	// 供选择、翻页用(只用于拼音输入法、扩展输入法和符号)
	BOOL m_bAllSyInvalid;	// 所有拼音都无效的标志(供显示用)
	int m_nSyLength;		// 单个拼音的字母数(总数)
	int m_nSyValidCount;	// 可以显示的拼音的数目(上行)
	int m_nSyCount;			// 查询出来的拼音的数目
	int m_nCurSy;			// 当前选中的拼音(上行，0-based)

	int m_nHzCount;			// 汉字的数目(下行，每行显示9个)
	int m_nCurPage;			// 当前页号(0-based)

	// 上下行的文字内容
	BYTE m_sUpperLine [33];	// 上行文字，32个字符
	BYTE m_sLowerLine [41];	// 下行文字，40个字符

	// 输入法返回集
	BYTE* m_sSyReturn;
	BYTE* m_sHzReturn;

	// 英文输入的按键等待超时标志
	int m_nTimeOut;		// 0未超时; 1超时

	// 在Edit控件上边显示
	BOOL m_bShowAboveTargetWnd;

public:
	OIME();
	virtual ~OIME();

	// 创建输入法窗口
	virtual BOOL Create (OApp* pApp);

	// 绘制输入法窗口
	virtual void Paint (LCD* pLCD);

	// 消息处理过了，返回1，未处理返回0
	virtual int Proc (OWindow* pWnd, ULONGLONG nMsg, ULONGLONG wParam, ULONGLONG lParam);

	// 是否可以处理控制按键(如果输入窗是空的，则不处理控制按键，而让OEdit窗口去处理)
	BOOL CanHandleControlKey();

	// 打开输入法窗口(打开显示，创建联系)
	BOOL OpenIME (OWindow* pWnd);

	// 关闭输入法窗口(关闭显示，断开联系)
	BOOL CloseIME (OWindow* pWnd);

	// 看输入法窗口是否处于打开状态
	BOOL IsIMEOpen();

private:
	// 处理定时器消息(只用于英文输入法)
	virtual void OnTimer (int nTimerID, int nInterval);

	// 向目标窗口发送字符(如果是英文，则只有c1，c2为0)
	void SendChar (BYTE c1, BYTE c2);

	// 根据数字件1~9选择字符并发送
	BYTE* LowerLineGetChar (int nKeyValue, int nLength);

	// 四种不同输入法的处理函数
	// 全键拼音输入法
	void PinyinProc (OWindow* pWnd, ULONGLONG nMsg, ULONGLONG wParam, ULONGLONG lParam);

	// 九键拼音输入法
	void Pinyin9KeyProc (OWindow* pWnd, ULONGLONG nMsg, ULONGLONG wParam, ULONGLONG lParam);

	// 英文输入法
	void English9KeyProc (OWindow* pWnd, ULONGLONG nMsg, ULONGLONG wParam, ULONGLONG lParam);

	// 符号输入法
	void PunctuationProc (OWindow* pWnd, ULONGLONG nMsg, ULONGLONG wParam, ULONGLONG lParam);

	// 根据m_sSyReturn、m_nSyLength、m_nSyCount更新上栏的字符串m_sUpperLine并设置m_nSyValidCount
	void RenewUpperLine();

	// 修改拼音后续处理
	BOOL RenewPinyin();

	// 修改九键拼音以后的后续处理
	BOOL RenewSy();

	// 根据m_sHzReturn、m_nHzCount、m_nCurPage更新下栏的字符串sLowerLine
	void RenewLowerLine();

	// 下行分页的向前翻页处理
	void LowerLinePageUp();

	// 下行分页的向后翻页处理
	void LowerLinePageDown();

	// 将按键消息转换成按键字符
	char MessageToKey (int nMsg);

	// 根据字母查找对应的数字键
	char CharToKey (char cc);

	// 根据数字键查找对应的字母(返回第一个)
	char KeyToChar (char cKey);

	// 查找该字母所在数字键组的下一个字母
	char KeyNextChar (char cc);
};

#endif // !defined(__OIME_H__)
