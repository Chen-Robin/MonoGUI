////////////////////////////////////////////////////////////////////////////////
// @file OIME_DB.cpp
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#include <fstream>
using namespace std;

#include "MonoGUI.h"

OIME_DB::OIME_DB ()
{
	m_nCurIME = IME_OFF;
	m_cSyLength = 0;
	memset (m_sSymbol, 0x0, sizeof(m_sSymbol));
	m_pPyLib = NULL;
	m_nPyCount = 0;
	m_pPyTable = NULL;
	m_pExLib = NULL;
	m_nExCount = 0;
	m_pExTable = NULL;
	m_pLxLib = NULL;
	m_nLxCount = 0;
	m_pLxTable = NULL;
	m_nSyCount = 0;
	m_pSy = NULL;
	m_nHzCount = 0;
	m_pHzTable = NULL;
	m_nLxHzCount = 0;
	m_pLxHzTable = NULL;
}

OIME_DB::~OIME_DB ()
{
	ReleaseDataTable();

	if (m_pSy) {
		delete[] m_pSy;
		m_pSy = NULL;
	}
	if (m_pHzTable) {
		delete[] m_pHzTable;
		m_pHzTable = NULL;
	}
	if (m_pLxHzTable) {
		delete[] m_pLxHzTable;
		m_pLxHzTable = NULL;
	}
}

// 释放数据表
void OIME_DB::ReleaseDataTable()
{
	if (m_pPyLib) {
		delete[] m_pPyLib;
		m_pPyLib = NULL;
	}
	if (m_pPyTable) {
		delete[] m_pPyTable;
		m_pPyTable = NULL;
	}
	if (m_pExLib) {
		delete[] m_pExLib;
		m_pExLib = NULL;
	}
	if (m_pExTable) {
		delete[] m_pExTable;
		m_pExTable = NULL;
	}
	if (m_pLxLib) {
		delete[] m_pLxLib;
		m_pLxLib = NULL;
	}
}

// 检查数据表文件是否已经存在
#if defined (RUN_ENVIRONMENT_WIN32)
BOOL OIME_DB::CheckDataTable()
{
	if (_access(PINYIN_LIB_FILE, 0) == -1)
		return FALSE;
	if (_access(EX_PINYIN_LIB_FILE, 0) == -1)
		return FALSE;
	if (_access(LIANXIANG_LIB_FILE, 0) == -1)
		return FALSE;
	if (_access(PINYIN_INDEX_FILE, 0) == -1)
		return FALSE;
	if (_access(EX_PINYIN_INDEX_FILE, 0) == -1)
		return FALSE;
	if (_access(LIANXIANG_INDEX_FILE, 0) == -1)
		return FALSE;

	return TRUE;
}
#endif // defined(RUN_ENVIRONMENT_WIN32)

// 初始化输入法库并创建索引表
BOOL OIME_DB::CreateDataTable ()
{
	if (	// 如果下列指针有非空的，说明已经初始化了或者初始化不成功
		(m_pPyLib	!= NULL) ||
		(m_pPyTable	!= NULL) ||
		(m_pExLib	!= NULL) ||
		(m_pExTable	!= NULL) ||
		(m_pLxLib	!= NULL) ||
		(m_pLxTable	!= NULL)) {
		DebugPrintf("IMEDB Can Not Be Init Twice !");
		return FALSE;
	}

	// 主要有6个步骤<1>~<6>
	// <1>打开py.db文件
	ifstream pinyinFile(PINYIN_LIB_FILE, ios::in | ios::binary);
	if (! pinyinFile.is_open()) {
		DebugPrintf("PinYin IME file py.db can not open !");
		return FALSE;
	}

	// 将py.db文件打开到m_pPyLib数组中
	streampos file_beg = pinyinFile.tellg();
	pinyinFile.seekg(0, ios::end);
	streampos file_end = pinyinFile.tellg();
	size_t file_len = file_end - file_beg;
	pinyinFile.seekg(0, ios::beg);

	m_pPyLib = new BYTE[file_len +1];
	m_pPyLib[file_len] = 0x0;
	pinyinFile.read((char*)m_pPyLib, file_len);
	pinyinFile.close();


	// <2>打开ex.db文件
	ifstream pinyinExFile(EX_PINYIN_LIB_FILE, ios::in | ios::binary);
	if (! pinyinExFile.is_open()) {
		DebugPrintf ("Extra PinYin IME file ex.db can not open !");
		return FALSE;
	}

	// 将ex.db文件打开到m_pExLib数组中
	file_beg = pinyinExFile.tellg();
	pinyinExFile.seekg(0, ios::end);
	file_end = pinyinExFile.tellg();
	file_len = file_end - file_beg;
	pinyinExFile.seekg(0, ios::beg);

	m_pExLib = new BYTE[file_len+1];
	m_pExLib[file_len] = 0x0;
	pinyinExFile.read((char*)m_pExLib, file_len);
	pinyinExFile.close();


	// <3>打开联想输入法词库lx.db文件
	ifstream lianxiangFile(LIANXIANG_LIB_FILE, ios::in | ios::binary);
	if (!lianxiangFile.is_open()) {
		DebugPrintf("Lengend Chinese Vocabulary file lx.db can not open !");
		return FALSE;
	}

	// 将ex.db文件打开到m_pExLib数组中
	file_beg = lianxiangFile.tellg();
	lianxiangFile.seekg(0, ios::end);
	file_end = lianxiangFile.tellg();
	file_len = file_end - file_beg;
	lianxiangFile.seekg(0, ios::beg);

	m_pLxLib = new BYTE[file_len + 1];
	m_pLxLib[file_len] = 0x0;
	lianxiangFile.read((char*)m_pLxLib, file_len);
	lianxiangFile.close();


	// 创建索引表
	int i = 0;
	int nLineStartPos;
	int nLineLength;

	// <4>创建标准拼音库的索引表
	// 计算拼音库的行数，要为每一行建立一个索引
	m_nPyCount = TotalLines((char*)m_pPyLib);
	if (m_nPyCount > 0)	{
		m_pPyTable = new _SY_INDEX_ITEM [m_nPyCount];	// 创建索引表所需要的内存空间
		memset (m_pPyTable, 0x0, (m_nPyCount*sizeof(_SY_INDEX_ITEM)));
	}
	else {
		DebugPrintf ("py.db invalid !");
		return FALSE;
	}

	for (i = 0; i < m_nPyCount; i++)
	{
		// 取出一行
		if (GetLine2((char*)m_pPyLib, &nLineStartPos, &nLineLength, i))
		{
			// 设置索引表
			memcpy (&(m_pPyTable[i].sSymbol), m_pPyLib + nLineStartPos, 6);	// 拷贝每一行前6个字节的拼音组合
			m_pPyTable[i].dwStart = nLineStartPos + 6;
			m_pPyTable[i].wLength = nLineLength - 6;
		}
	}
	// 将创建好的索引表写入py.idx文件
	ofstream pinyinIndexFile(PINYIN_INDEX_FILE, ios::out | ios::binary);
	pinyinIndexFile.write((char*)m_pPyTable, m_nPyCount * sizeof(_SY_INDEX_ITEM));
	pinyinIndexFile.close();
	

	// <5>创建方言扩展库的索引表
	// 计算方言扩展库的行数，要为每一行建立一个索引
	m_nExCount = TotalLines((char*)m_pExLib);
	if (m_nExCount > 0) {
		m_pExTable = new _SY_INDEX_ITEM[m_nExCount];	// 创建索引表所需要的内存空间
		memset(m_pExTable, 0x0, (m_nExCount * sizeof(_SY_INDEX_ITEM)));
	}
	else {
		DebugPrintf("ex.db invalid !");
		return FALSE;
	}

	for (i = 0; i < m_nExCount; i++)
	{
		// 取出一行
		if (GetLine2((char*)m_pExLib, &nLineStartPos, &nLineLength, i))
		{
			// 设置索引表
			memcpy (&(m_pExTable[i].sSymbol), m_pExLib + nLineStartPos, 6); // 拷贝每一行前6个字节的拼音组合
			m_pExTable[i].dwStart = nLineStartPos + 6;
			m_pExTable[i].wLength = nLineLength - 6;
		}
	}
	// 将创建好的索引表写入ex.idx文件
	ofstream pinyinExIndexFile(EX_PINYIN_INDEX_FILE, ios::out | ios::binary);
	pinyinExIndexFile.write((char*)m_pExTable, m_nExCount * sizeof(_SY_INDEX_ITEM));
	pinyinExIndexFile.close();


	// <6>创建联想词库的索引表
	m_nLxCount = TotalLines((char*)m_pLxLib);
	if (m_nLxCount > 0) {
		m_pLxTable = new _HZ_INDEX_ITEM[m_nLxCount];	// 创建索引表所需要的内存空间
		memset(m_pLxTable, 0x0, (m_nLxCount * sizeof(_HZ_INDEX_ITEM)));
	}
	else {
		DebugPrintf("lx.db invalid !");
		return FALSE;
	}

	for (i = 0; i < m_nLxCount; i++)
	{
		// 取出一行
		if (GetLine2((char*)m_pLxLib, &nLineStartPos, &nLineLength, i))
		{
			// 设置索引表
			memcpy (&(m_pLxTable[i].sCharacter), m_pLxLib + nLineStartPos, 2);	// 拷贝每一行前2个字节的拼音组合
			m_pLxTable[i].dwStart = nLineStartPos + 2;
			m_pLxTable[i].wLength = nLineLength - 2;
		}
	}
	// 将创建好的索引表写入lx.idx文件
	ofstream lianxiangIndexFile(LIANXIANG_INDEX_FILE, ios::out | ios::binary);
	lianxiangIndexFile.write((char*)m_pLxTable, m_nLxCount * sizeof(_HZ_INDEX_ITEM));
	lianxiangIndexFile.close();

	return TRUE;
}

BOOL OIME_DB::LoadDataTable ()
{
	ReleaseDataTable();

	// <1>打开拼音输入法库
	ifstream pinyinFile(PINYIN_LIB_FILE, ios::in | ios::binary);
	if (!pinyinFile.is_open()) {
		DebugPrintf("ERROR: OIME_DB::LoadDataTable Can't Open py.db\n");
		return FALSE;
	}

	streampos file_beg = pinyinFile.tellg();
	pinyinFile.seekg(0, ios::end);
	streampos file_end = pinyinFile.tellg();
	size_t file_len = file_end - file_beg;
	pinyinFile.seekg(0, ios::beg);

	m_pPyLib = new BYTE[file_len + 1];
	m_pPyLib[file_len] = 0x0;
	pinyinFile.read((char*)m_pPyLib, file_len);
	pinyinFile.close();

	// 打开扩展输入法库
	ifstream pinyinExFile(EX_PINYIN_LIB_FILE, ios::in | ios::binary);
	if (!pinyinExFile.is_open()) {
		DebugPrintf("ERROR: OIME_DB::LoadDataTable Can't Open ex.db\n");
		return FALSE;
	}

	file_beg = pinyinExFile.tellg();
	pinyinExFile.seekg(0, ios::end);
	file_end = pinyinExFile.tellg();
	file_len = file_end - file_beg;
	pinyinExFile.seekg(0, ios::beg);

	m_pExLib = new BYTE[file_len + 1];
	m_pExLib[file_len] = 0x0;
	pinyinExFile.read((char*)m_pExLib, file_len);
	pinyinExFile.close();

	// 打开联想词库
	ifstream lianxiangFile(LIANXIANG_LIB_FILE, ios::in | ios::binary);
	if (!lianxiangFile.is_open()) {
		DebugPrintf("ERROR: OIME_DB::LoadDataTable Can't Open lx.db\n");
		return FALSE;
	}

	file_beg = lianxiangFile.tellg();
	lianxiangFile.seekg(0, ios::end);
	file_end = lianxiangFile.tellg();
	file_len = file_end - file_beg;
	lianxiangFile.seekg(0, ios::beg);

	m_pLxLib = new BYTE[file_len + 1];
	m_pLxLib[file_len] = 0x0;
	lianxiangFile.read((char*)m_pLxLib, file_len);
	lianxiangFile.close();

	// 打开拼音输入法索引
	ifstream pinyinIndexFile(PINYIN_INDEX_FILE, ios::in | ios::binary);
	if (!pinyinIndexFile.is_open()) {
		DebugPrintf("ERROR: OIME_DB::LoadDataTable Can't Open py.idx\n");
		return FALSE;
	}

	m_nPyCount = IME_PY_LINES;
	m_pPyTable = new _SY_INDEX_ITEM[m_nPyCount];
	pinyinIndexFile.read((char*)m_pPyTable, m_nPyCount * sizeof(_SY_INDEX_ITEM));
	size_t nReadCount = pinyinIndexFile.gcount();
	pinyinIndexFile.close();
	if (nReadCount != m_nPyCount * sizeof(_SY_INDEX_ITEM)) {
		DebugPrintf("ERROR: OIME_DB::LoadDataTable py.idx Read Error.\n");
		printf ("sizeof(_SY_INDEX_ITEM)=%ld, SHOULD BE 12 !!!\n", sizeof(_SY_INDEX_ITEM));
		return FALSE;
	}

	// 打开扩展输入法索引
	ifstream pinyinExIndexFile(EX_PINYIN_INDEX_FILE, ios::in | ios::binary);
	if (!pinyinExIndexFile.is_open()) {
		DebugPrintf("ERROR: OIME_DB::LoadDataTable Can't Open ex.idx\n");
		return FALSE;
	}

	m_nExCount = IME_EX_LINES;
	m_pExTable = new _SY_INDEX_ITEM[m_nExCount];
	pinyinExIndexFile.read((char*)m_pExTable, m_nExCount * sizeof(_SY_INDEX_ITEM));
	nReadCount = pinyinExIndexFile.gcount();
	pinyinExIndexFile.close();
	if (nReadCount != m_nExCount * sizeof(_SY_INDEX_ITEM)) {
		DebugPrintf("ERROR: OIME_DB::LoadDataTable ex.idx Read Error.\n");
		printf ("sizeof(_SY_INDEX_ITEM)=%ld, SHOULD BE 12 !!!\n", sizeof(_SY_INDEX_ITEM));
		return FALSE;
	}

	// 打开联想词库索引
	ifstream lianxiangIndexFile(LIANXIANG_INDEX_FILE, ios::in | ios::binary);
	if (!lianxiangIndexFile.is_open()) {
		DebugPrintf("ERROR: OIME_DB::LoadDataTable Can't Open lx.idx\n");
		return FALSE;
	}

	m_nLxCount = IME_LX_LINES;
	m_pLxTable = new _HZ_INDEX_ITEM[m_nLxCount];
	lianxiangIndexFile.read((char*)m_pLxTable, m_nLxCount * sizeof(_HZ_INDEX_ITEM));
	nReadCount = lianxiangIndexFile.gcount();
	lianxiangIndexFile.close();
	if (nReadCount != m_nLxCount * sizeof(_HZ_INDEX_ITEM)) {
		DebugPrintf("ERROR: OIME_DB::LoadDataTable lx.idx Read Error.\n");
		printf ("sizeof(_HZ_INDEX_ITEM)=%ld, SHOULD BE 8 !!!\n", sizeof(_HZ_INDEX_ITEM));
		return FALSE;
	}
    
	return TRUE;
}

// 设定当前输入法：标准拼音或方言扩展
int OIME_DB::SetCurIME (int nIME)
{
	int nOldIME = m_nCurIME;
	if (nIME == IME_PY || nIME == IME_FY || nIME == IME_9PY || nIME == IME_9FY)
	{
		m_nCurIME = nIME;
	}
	return nOldIME;
}

// 向输入表添加一个字符
BOOL OIME_DB::SyAdd (char ch)
{
	// 只允许输入数字
	if ((ch < '0') || (ch > '9')) {
		return FALSE;
	}

	if ((m_cSyLength >= 0) && (m_cSyLength < 6))
	{
		m_sSymbol [m_cSyLength] = ch;
		m_cSyLength ++;
		return TRUE;
	}
	return FALSE;
}

// 删除输入表的最后一个字符
BOOL OIME_DB::SyRemove ()
{
	if (m_cSyLength > 0)
	{
		m_cSyLength --;
		m_sSymbol [m_cSyLength] = '\0';
		return TRUE;
	}
	return FALSE;
}

// 清空输入表
BOOL OIME_DB::SyRemoveAll ()
{
	memset (m_sSymbol, 0x0, sizeof(m_sSymbol));
	m_cSyLength = 0;
	return TRUE;
}

// 得到输入表的长度
int OIME_DB::GetSyLength ()
{
	return (int)m_cSyLength;
}

// 根据拼音输入表查找匹配的拼音
int OIME_DB::GetSy (BYTE** pSy)
{
	// 如果未设定输入法则退出查询
	if (m_nCurIME == IME_OFF) {
		return 0;
	}
	if (m_cSyLength == 0) {
		return 0;
	}
	if ((m_nCurIME == IME_PY || m_nCurIME == IME_FY)
		&& (m_pPyTable == NULL)) {
		return 0;
	}
	if ((m_nCurIME == IME_FY || m_nCurIME == IME_9FY) 
		&& (m_pExTable == NULL)) {
		return 0;
	}

	// 清空返回集
	if (m_pSy != NULL)
	{
		delete [] m_pSy;
		m_pSy = NULL;
	}

	_SY_INDEX_ITEM* pTable = NULL;	// 要查询的输入法表
	int nTableLength = 0;			// 输入法表的长度

	if (m_nCurIME == IME_PY || m_nCurIME == IME_9PY)
	{
		pTable = m_pPyTable;
		nTableLength = m_nPyCount;
	}
	else if (m_nCurIME == IME_FY || m_nCurIME == IME_9FY)
	{
		pTable = m_pExTable;
		nTableLength = m_nExCount;
	}

	// 根据第一个字母，生成第一次查询的输出表
	m_pSy = new BYTE [nTableLength * 8];
	memset (m_pSy, 0x0, (nTableLength * 8));
	m_nSyCount = 0;
	// 注：这样做空间肯定有很多空余，但是在下次查询的时候会被删掉
	// 之所以不采用一条一条添加的方式，是为了换取更高的速度

	char cKey [4];
	GetKeySymbol (cKey, m_sSymbol[0]);	// 得到数字键所对应的4个英文字母

	int i;
	for (i = 0; i < nTableLength; i++)
	{
		char cc = pTable[i].sSymbol[0];	// 取得索引表中相应位置的字符
		if (
			((cc == cKey[0]) ||			// 如果索引表中的字母与按键对应的字母相吻合
			 (cc == cKey[1]) ||
			 (cc == cKey[2]) ||
			 (cc == cKey[3]))
			 && (cc != 0x0)
		   )
		{
			memcpy ((m_pSy + m_nSyCount * 8), pTable[i].sSymbol, 6);
			m_nSyCount ++;
		}
	}

	// 后面的查询均以前一次查询的输出集作为输入集
	for (i = 1; i < m_cSyLength; i++)
	{
		BYTE* pTempTable = new BYTE [m_nSyCount * 8];
		memset (pTempTable, 0x0, (m_nSyCount * 8));
		int nTempCount = 0;

		char sKey [4];
		GetKeySymbol (sKey, m_sSymbol[i]);

		int j;
		for (j = 0; j < m_nSyCount; j++)
		{
			char cc = m_pSy[j*8+i];	// 取得索引表中相应位置的字符
			if (
				((cc == sKey[0]) ||	// 如果索引表中的字母与按键对应的字母相吻合
				 (cc == sKey[1]) ||
				 (cc == sKey[2]) ||
				 (cc == sKey[3]))
				 && (cc != 0x0)
			   )
			{
				memcpy ((pTempTable + nTempCount * 8), &(m_pSy[j*8]), 6);
				nTempCount ++;
			}
		}

		if (m_pSy != NULL)
		{
			delete [] m_pSy;
			m_pSy = NULL;
		}

		m_pSy = pTempTable;
		m_nSyCount = nTempCount;
	}

	// 设置拼音组合的匹配值('0'不完全匹配；'1'完全匹配)
	char cFirstCh = 0x0;
	for (i = 0; i < m_nSyCount; i++)
	{
		if (m_cSyLength == 6)
		{
			// 输入集达到最大长度6个字母，设置为完全匹配
			m_pSy[i*8+6] = '1';
		}
		else if (m_cSyLength == 1)
		{
			// 如果输入集只有1个字母，则将第一个字母不同的设置为完全匹配
			if (m_pSy[i*8] != cFirstCh)
			{
				m_pSy[i*8+6] = '1';
				cFirstCh = m_pSy[i*8];
			}
			else
			{
				m_pSy[i*8+6] = '0';
			}
		}
		else
		{
			if (m_pSy[i*8+m_cSyLength] == 0x20)	// 看后面是否空格键，如果是，说明后面没有其他字母了
				m_pSy[i*8+6] = '1';
			else
				m_pSy[i*8+6] = '0';
		}
	}

	*pSy = m_pSy;
	return m_nSyCount;
}

// 根据拼音查找汉字
int OIME_DB::GetHz (BYTE* pInput, BYTE** pResult)
{
	// 如果未设定输入法则退出查询
	if (m_nCurIME == IME_OFF) {
		return 0;
	}
	if ((m_nCurIME == IME_PY || m_nCurIME == IME_9PY)
		&& ((m_pPyLib == NULL) || (m_pPyTable == NULL))) {
		return 0;
	}
	if ((m_nCurIME == IME_FY || m_nCurIME == IME_9FY)
		&& ((m_pExLib == NULL) || (m_pExTable == NULL))) {
		return 0;
	}

	BYTE* pLib = NULL;
	_SY_INDEX_ITEM* pTable = NULL;	// 要查询的输入法表
	int nTableLength = 0;			// 输入法表的长度

	if (m_nCurIME == IME_PY || m_nCurIME == IME_9PY)
	{
		pLib = m_pPyLib;
		pTable = m_pPyTable;
		nTableLength = m_nPyCount;
	}
	else if (m_nCurIME == IME_FY || m_nCurIME == IME_9FY)
	{
		pLib = m_pExLib;
		pTable = m_pExTable;
		nTableLength = m_nExCount;
	}
	else
	{
		return 0;
	}

	// 如果输入的拼音串只有一个字母，则使用模糊查询
	int nResult = -1;
	if (pInput[1] == 0x20)
	{
		int i;
		for (i = 0; i < nTableLength; i++)
		{
			if (pTable[i].sSymbol[0] == pInput[0])
			{
				nResult = i;	// 找到了
				break;
			}
		}
	}
	else
	{
		// 使用二分搜索法
		nResult = SearchHz (0, nTableLength, pTable, pInput);
	}

	if (m_pHzTable != NULL)
	{
		delete [] m_pHzTable;
		m_pHzTable = NULL;
	}

	if (nResult == -1)
		return 0;	// 表示没有找到

	m_nHzCount = (int)(pTable[nResult].wLength);
	DWORD dwOffset = pTable[nResult].dwStart;

	m_pHzTable = new BYTE [m_nHzCount+1];
	memset (m_pHzTable, 0x0, m_nHzCount+1);
	memcpy (m_pHzTable, &pLib[dwOffset], m_nHzCount);

	*pResult = m_pHzTable;
	return (m_nHzCount / 2);	// 返回汉字的数目
}

// 根据汉字查找联想字
int OIME_DB::GetLx (BYTE* pInput, BYTE** pResult)
{
	if (m_pLxTable == NULL) {
		return 0;
	}
	if ((m_pLxLib == NULL) || (m_pLxTable == NULL)) {
		return 0;
	}

	// 使用二分搜索法
	int nResult = SearchLx (0, m_nLxCount, m_pLxTable, pInput);

	if (m_pLxHzTable != NULL)
	{
		delete [] m_pLxHzTable;
		m_pLxHzTable = NULL;
	}

	if (nResult == -1)
		return 0;	// 表示没有找到

	m_nLxHzCount = (int)(m_pLxTable[nResult].wLength);
	DWORD dwOffset = m_pLxTable[nResult].dwStart;

	m_pLxHzTable = new BYTE [m_nLxHzCount+1];
	memset (m_pLxHzTable, 0x0, m_nLxHzCount+1);
	memcpy (m_pLxHzTable, &m_pLxLib[dwOffset], m_nLxHzCount);

	*pResult = m_pLxHzTable;
	return (m_nLxHzCount / 2);		// 返回汉字的数目
}

// 得到数字键所对应的4个英文字母
void OIME_DB::GetKeySymbol (char* pKey, char cNum)
{
	switch (cNum)
	{
	case '0':
		memcpy (pKey, Key0, 4);
		break;
	case '1':
		memcpy (pKey, Key1, 4);
		break;
	case '2':
		memcpy (pKey, Key2, 4);
		break;
	case '3':
		memcpy (pKey, Key3, 4);
		break;
	case '4':
		memcpy (pKey, Key4, 4);
		break;
	case '5':
		memcpy (pKey, Key5, 4);
		break;
	case '6':
		memcpy (pKey, Key6, 4);
		break;
	case '7':
		memcpy (pKey, Key7, 4);
		break;
	case '8':
		memcpy (pKey, Key8, 4);
		break;
	case '9':
		memcpy (pKey, Key9, 4);
		break;
	}
}

// 二分搜索法的搜索子程序
// iStart:开始位置；iEnd:结束位置；pIndex:索引字符串；pString:参加比较的字符串
// 用拼音搜索汉字
int OIME_DB::SearchHz (int nStart, int nEnd, _SY_INDEX_ITEM* pIndex, BYTE* pString)
{
	int nMid = 0;
	int nResult = 0;

	while (nStart <= nEnd)
	{
		nMid = (nStart + nEnd) / 2;
		nResult = memcmp (pIndex[nMid].sSymbol, pString, 6);

		if (nResult == 0)
			return nMid;    // 找到啦！

		if (nResult > 0)
			nEnd = nMid - 1;
		else
			nStart = nMid + 1;
	}

	return (-1);           // 没找到
}

// 用汉字搜索联想汉字
int OIME_DB::SearchLx (int nStart, int nEnd, _HZ_INDEX_ITEM* pIndex, BYTE* pString)
{
	int nMid = 0;
	int nResult = 0;

	while (nStart <= nEnd)
	{
		nMid = (nStart + nEnd) / 2;
		nResult = memcmp (pIndex[nMid].sCharacter, pString, 2);

		if (nResult == 0)
			return nMid;    // 找到啦！

		if (nResult > 0)
			nEnd = nMid - 1;
		else
			nStart = nMid + 1;
	}

	return (-1);           // 没找到
}

/* END */
