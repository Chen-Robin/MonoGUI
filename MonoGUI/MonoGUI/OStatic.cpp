////////////////////////////////////////////////////////////////////////////////
// @file OStatic.cpp
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#include "MonoGUI.h"

OStatic::OStatic ()
	: OWindow(self_type)
{
	m_pImage = NULL;
}

OStatic::~OStatic ()
{
	m_pImage = NULL;
}

// 创建静态文本
BOOL OStatic::Create (OWindow* pParent,WORD wStyle,WORD wStatus,int x,int y,int w,int h,int ID)
{
	if (!OWindow::Create(pParent, wStyle, wStatus, x, y, w, h, ID)) {
		DebugPrintf( "Create Static Control Failure. \n" );
		return FALSE;
	}

	//DebugPrintf( "Create Static Control Success. \n" );
	m_wStatus |= WND_STATUS_INVALID;	// 静态文本不能获得焦点
	return TRUE;
}

// 调入图片
BOOL OStatic::SetImage (BW_IMAGE* pImage)
{
	CHECK_TYPE_RETURN;

	if (pImage == NULL) {
		return FALSE;
	}

	m_pImage = pImage;

	return TRUE;
}

void OStatic::Paint (LCD* pLCD)
{
	CHECK_TYPE;

	// 如果不可见，则什么也不绘制
	if (! IsWindowVisible ()) {
		return;
	}

	// 图形的绘制
	if (m_pImage != NULL)
	{
		// 图片宽度按字节对齐
		int nImageW = ((m_w + 7) / 8) * 8;
		LCD lcd(*m_pImage);

		pLCD->BitBlt(m_x,m_y,m_w,m_h,
				lcd,0,0,LCD_MODE_NORMAL);

		return;
	}

	int nTitleLength = 0;	// 显示字符串长度限制

	int crBk = 1;
	int crFr = 0;

	// 如果有边框，则绘制边框
	if ((m_wStyle & WND_STYLE_NO_BORDER) == 0)
	{
		if ((m_wStyle & WND_STYLE_ROUND_EDGE) > 0)
		{
			// 绘制圆角风格的边框
			pLCD->DrawImage(m_x, m_y+7, 6, 6, g_4color_Button_Normal, 0, 0, LCD_MODE_NORMAL);
			pLCD->DrawImage(m_x+m_w-6, m_y+7, 6, 6, g_4color_Button_Normal, 6, 0, LCD_MODE_NORMAL);
			pLCD->DrawImage(m_x, m_y+m_h-6, 6, 6, g_4color_Button_Normal, 0, 6, LCD_MODE_NORMAL);
			pLCD->DrawImage(m_x+m_w-6, m_y+m_h-6, 6, 6, g_4color_Button_Normal, 6, 6, LCD_MODE_NORMAL);
			pLCD->HLine    (m_x+6, m_y+7, m_w-12, crFr);
			pLCD->HLine    (m_x+6, m_y+m_h-1, m_w-12, crFr);
			pLCD->VLine    (m_x, m_y+13, m_h-19, crFr);
			pLCD->VLine    (m_x+m_w-1, m_y+13, m_h-19, crFr);
		}
		else
		{
			// 绘制普通风格的边框
			pLCD->HLine (m_x, m_y+7, m_w, crFr);
			pLCD->HLine (m_x, m_y+m_h-1, m_w, crFr);
			pLCD->VLine (m_x, m_y+7, m_h-7, crFr);
			pLCD->VLine (m_x+m_w-1, m_y+7, m_h-7, crFr);
		}
		// 绘制文字
		if (GetTextLength() > 0)
		{
			pLCD->VLine (m_x+5, m_y, 12, 0);	// 字前留空
		}

		nTitleLength = GetDisplayLimit (m_sCaption, m_w-6);
		pLCD->TextOut (m_x+6,m_y+2,(BYTE*)m_sCaption,nTitleLength, LCD_MODE_NORMAL);
	}
	else
	{
		// 无边框时文字前不留空
		nTitleLength = GetDisplayLimit (m_sCaption, m_w);
		pLCD->TextOut (m_x,m_y+2,(BYTE*)m_sCaption,nTitleLength, LCD_MODE_NORMAL);
	}
}

// Static控件什么也不处理
int OStatic::Proc (OWindow* pWnd, ULONGLONG nMsg, ULONGLONG wParam, ULONGLONG lParam)
{
	CHECK_TYPE_RETURN;
	return 0;
}

/* END */
