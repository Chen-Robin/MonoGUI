////////////////////////////////////////////////////////////////////////////////
// @file DlgShowList.cpp
// @author Amos Liu (liuxinouc@126.com)
// @date 2007/10/10
// @brief The misc portable functions.
//
// Copyright (C) 2002 Situ Experimental Design Studio (P.R.China)
//
// This file is a part of MonoGUI, an Black and White Graphic User Interface.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////
#include "stdafx.h"

static char* l_arChinaCities[] =
{
	"北京",
	"上海",
	"南京",
	"青岛",
	"深圳",
	"福州",
	"吉林",
	"成都",
	"日照",
	"济南",
	"泰安",
	"天津"
};
#define CHINA_CITY_COUNT   (sizeof(l_arChinaCities) / sizeof(l_arChinaCities[0]))

static char* l_arAmericaCities[] =
{
	"华盛顿",
	"纽约",
	"芝加哥",
	"洛杉矶",
	"费城",
	"波士顿",
	"旧金山",
	"休斯敦",
	"亚特兰大"
};
#define AMERICA_CITY_COUNT   (sizeof(l_arAmericaCities) / sizeof(l_arAmericaCities[0]))

static char* l_arEnglandCities[] =
{
	"伦敦",
	"爱丁堡",
	"加的夫",
	"贝尔法斯特",
	"伯明翰",
	"利物浦",
	"牛津",
	"剑桥",
	"格拉斯哥",
	"谢菲尔德",
	"曼彻斯特",
	"普利茅斯"
};
#define ENGLAND_CITY_COUNT   (sizeof(l_arEnglandCities) / sizeof(l_arEnglandCities[0]))
//////////////////////////////////////////////////////////////////////
CDlgShowList::CDlgShowList()
{
}

CDlgShowList::~CDlgShowList()
{
}

// 初始化
void CDlgShowList::Init()
{
	OList* pList = (OList *)FindChildByID (102);
	pList->AddString ("中国");
	pList->AddString ("美国");
	pList->AddString ("英国");


	pList = (OList*) FindChildByID (103);
	int i;
	for (i = 0; i < CHINA_CITY_COUNT; i++) {
		pList->AddString (l_arChinaCities[i]);
	}
}

// 消息处理过了，返回1，未处理返回0
int CDlgShowList::Proc (OWindow* pWnd, ULONGLONG nMsg, ULONGLONG wParam, ULONGLONG lParam)
{
	ODialog::Proc (pWnd, nMsg, wParam, lParam);

	if (pWnd = this)
	{
		if (nMsg == OM_DATACHANGE)
		{
			if (wParam == 102) {

				OList* pList = (OList *)FindChildByID (102);
				int nCurSel = pList->GetCurSel();

				if (nCurSel != -1) {
					int i;
					switch (nCurSel) {
					case 0:
						{
							pList = (OList*) FindChildByID (103);
							pList->RemoveAll();
							for (i = 0; i < CHINA_CITY_COUNT; i++) {
								pList->AddString (l_arChinaCities[i]);
							}
						}
						break;
					case 1:
						{
							pList = (OList*) FindChildByID (103);
							pList->RemoveAll();
							for (i = 0; i < AMERICA_CITY_COUNT; i++) {
								pList->AddString (l_arAmericaCities[i]);
							}
						}
						break;
					case 2:
						{
							pList = (OList*) FindChildByID (103);
							pList->RemoveAll();
							for (i = 0; i < ENGLAND_CITY_COUNT; i++) {
								pList->AddString (l_arEnglandCities[i]);
							}
						}
						break;
					}
				}
			}
		}

		if (nMsg == OM_NOTIFY_PARENT)
		{
			switch (wParam)
			{
			case 104:
				{
					OList* pStateList = (OList *)FindChildByID (102);
					OList* pCityList  = (OList *)FindChildByID (103);

					int nStateIndex = pStateList->GetCurSel();
					int nCityIndex  = pCityList->GetCurSel();

					if (-1 != nStateIndex || -1 != nCityIndex) {
						char state[LIST_TEXT_MAX_LENGTH];
						char city[LIST_TEXT_MAX_LENGTH];
						memset (state, 0x0, sizeof(state));
						memset (city, 0x0, sizeof(city));

						pStateList->GetString(nStateIndex, state);
						pCityList->GetString(nCityIndex, city);

						char info[LIST_TEXT_MAX_LENGTH * 2];
						snprintf (info, sizeof(info), "您选中的国家是：\n%s\n您选择的城市是：\n%s", state, city);
						OMsgBox (this, "信息", info,
							OMB_INFORMATION | OMB_SOLID | OMB_ROUND_EDGE, 60);
					}
					else {
						OMsgBox (this, "信息", "请您在列表中做出选择",
							OMB_INFORMATION | OMB_SOLID | OMB_ROUND_EDGE, 60);
					}
				}
				break;

			case 105:
				{
					// 退出按钮
					O_MSG msg;
					msg.pWnd = this;
					msg.message = OM_CLOSE;
					msg.wParam = 0;
					msg.lParam = 0;
					m_pApp->PostMsg (&msg);
				}
				break;
			}
		}
	}

	return 1;
}

/* END */