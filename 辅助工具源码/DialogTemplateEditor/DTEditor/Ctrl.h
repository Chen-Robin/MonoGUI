// Ctrl.h: interface for the CCtrl class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CTRL_H__F78F28DF_41CD_48D8_A0AC_2EEDDEB87FA5__INCLUDED_)
#define AFX_CTRL_H__F78F28DF_41CD_48D8_A0AC_2EEDDEB87FA5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define LEVER_RECT_W   7
#define LEVER_RECT_H   7
class CCtrl  
{
// 成员变量
public:
	CONTROL_INFO  m_ControlInfo;
private:
	int		m_nTab;			// Tab序号
	BOOL	m_bSelect;		// 是否处于选中状态
	CRect   m_LeverTop;     // 调节控件上边位置的矩形框
	CRect   m_LeverLeft;    // 调节控件左端位置的矩形框
	CRect   m_LeverBottom;  // 调节控件下边位置的矩形框
	CRect   m_LeverRight;   // 调节控件右端位置的矩形框

// 成员函数
public:
	CCtrl(int nType);
	virtual ~CCtrl();

protected:
	int  GetX1();
	int  GetX2();
	int  GetY1();
	int  GetY2();
	int  SetX1(int x1);
	int  SetX2(int x2);
	int  SetY1(int y1);
	int  SetY2(int y2);

public:
	int  GetXPos();
	int  GetYPos();
	int  GetWidth();
	int  GetHeight();
	void SetWidth(int w);
	void SetHeight(int h);
	void ResizeLevelRects();                    // 重新调整调节矩形的位置
	void SetPos(int x, int y);                  // 设置控件位置
	void Show(CDC* pdc, BOOL bShowTabNum);		// 显示
    BOOL IsDialog();                            // 当前控件是不是对话框
	BOOL PtInArea(int x, int y);				// 选择
    BOOL RectInArea(CRect rc);                  // 判断是否与矩形相交
	BOOL SetSelState(BOOL bState);				// TRUE:选中；FALSE:未选中
	BOOL GetSelState();							// 得到选择状态
	int  SetTabNum(int nTabNum);				// 设置Tab序号
	int  GetTabNum();							// 取得Tab序号
	BOOL Open(CString strInfo);					// 从字符串恢复控件属性
	CString Save();								// 将控件属性存入字符串

	// 设置改变尺寸操作的鼠标光标
	// 参数说明：
	// 返回值：xy坐标在控件范围之内：TRUE； 在控件范围之外：FALSE；
	// 如果返回FALSE，则iCursor和iMode的值都被设置为0；
	// iCursor：1：全向移动光标；2：上下移动光标；3：左右移动光标
	// iMode：1：移动；2：调节上边；3：调节下边；4：调节左边；5：调节右边
	BOOL SetModifySizeCursor(int* pnCursor, int* pnMode, int x, int y);

	// 处理鼠标拖拽改变尺寸
	void MouseDrag(int nMode, int nDX, int nDY);
};

#endif // !defined(AFX_CTRL_H__F78F28DF_41CD_48D8_A0AC_2EEDDEB87FA5__INCLUDED_)
